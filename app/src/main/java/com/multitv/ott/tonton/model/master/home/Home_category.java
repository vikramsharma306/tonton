package com.multitv.ott.tonton.model.master.home;

import java.util.ArrayList;

/**
 * Created by root on 24/10/16.
 */
public class Home_category {
    public String cat_id;
    public String cat_name;
    public ArrayList<ContentHome> cat_cntn = new ArrayList<>();
}
