package com.multitv.ott.tonton.model.master;

/**
 * Created by naseeb on 11/8/2017.
 */

public class Master {
    public ApiType get;
    public ApiType post;
    public String subscription;
    public String analytics;
    public String acknowledgement;

    public class ApiType {
        public String low;
        public String medium;
        public String high;
    }
}
