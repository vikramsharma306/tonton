package com.multitv.ott.tonton.model.master.home;

import java.util.List;

public class Result {
    public Dashboard dashboard;
    public Integer display_count;
    public Integer display_offset;
    public Version version;

}