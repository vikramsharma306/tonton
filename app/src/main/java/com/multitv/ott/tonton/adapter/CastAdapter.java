package com.multitv.ott.tonton.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.multitv.ott.tonton.R;
import com.multitv.ott.tonton.model.master.cast.CastModel;
import com.multitv.ott.tonton.model.master.contentdetail.ContentDetails;
import com.multitv.ott.tonton.model.master.home.ContentHome;
import com.multitv.ott.tonton.uttils.ScreenUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHolder> {
    private List<ContentDetails.Content.Meta.AllCast> castList = new ArrayList<>();
    private Context context;

    public CastAdapter(Context context, List<ContentDetails.Content.Meta.AllCast> castList) {
        this.context = context;
        this.castList = castList;
    }

    @NonNull
    @Override
    public CastAdapter.CastViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cast_adapter, null);
        CastAdapter.CastViewHolder mh = new CastAdapter.CastViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(@NonNull CastAdapter.CastViewHolder holder, int position) {
        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, context.getResources().getDisplayMetrics());

        holder.parentLayout.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(context) / 4 - widthAndHeightOfIcon, RecyclerView.LayoutParams.WRAP_CONTENT));

        ContentDetails.Content.Meta.AllCast castData = castList.get(position);

        holder.nameTv.setText(castData.name);
        holder.detailTv.setText(castData.name);
//        holder.circleImageView.setImageResource(castData.imag);


        if (castData.image!=null && !TextUtils.isEmpty(castData.image)){
            Picasso.with(context)
                    .load(castData.image)
                    .into(holder.circleImageView);

        }else{
            Picasso.with(context)
                    .load(R.mipmap.logo_user)
                    .error(R.mipmap.logo_user)
                    .into(holder.circleImageView);
        }
    }

    @Override
    public int getItemCount() {
        return castList.size();
    }

    public class CastViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout parentLayout;
        private CircleImageView circleImageView;
        private TextView detailTv, nameTv;

        public CastViewHolder(View view) {
            super(view);
            parentLayout = view.findViewById(R.id.parentLayout);
            circleImageView = view.findViewById(R.id.circleImageView);
            detailTv = view.findViewById(R.id.detailTv);
            nameTv = view.findViewById(R.id.nameTv);
        }

    }
}
