package com.multitv.ott.tonton.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class ThinEditText extends AppCompatEditText {

    public ThinEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ThinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ThinEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/thin.ttf");
        setTypeface(tf);
    }

}