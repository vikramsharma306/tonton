package com.multitv.ott.tonton.model.master.live;

import com.multitv.ott.tonton.model.master.home.ContentHome;


import java.util.ArrayList;
import java.util.List;

public class LiveParent {
    public Result result;

    public class Result {
        public String version;
        public int offset;
        public int totalcount;
        public List<ContentHome> live = new ArrayList<>();
    }
}