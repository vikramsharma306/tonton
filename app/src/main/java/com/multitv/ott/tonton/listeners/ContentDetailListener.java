package com.multitv.ott.tonton.listeners;

public interface ContentDetailListener {
    void onContentClick(String id);
}
