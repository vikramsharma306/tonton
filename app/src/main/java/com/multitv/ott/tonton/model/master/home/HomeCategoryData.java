package com.multitv.ott.tonton.model.master.home;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyberlinks on 16/3/17.
 */

public class HomeCategoryData {
    public Integer offset;
    public int totalCount;
    public String version;
    public List<ContentHome> content = new ArrayList<>();
}
