package com.multitv.ott.livistore.utils

import java.util.regex.Pattern

class VaildationHelper {
    fun isValidMobile(phone: String): Boolean {
        val p = Pattern.compile(regexString)
        val m = p.matcher(phone)
        return (m.find() && m.group().equals(phone))
    }

    var regexString = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}\$"
    fun isValidEmail(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}