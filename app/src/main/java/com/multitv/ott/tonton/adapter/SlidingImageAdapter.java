package com.multitv.ott.tonton.adapter;

import android.os.Bundle;
import android.os.Parcelable;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.multitv.ott.tonton.model.master.home.ContentHome;

import java.util.ArrayList;

/**
 * Created by cyberlinks on 18/1/17.
 */

public class SlidingImageAdapter extends FragmentStatePagerAdapter {

    private ArrayList<ContentHome> featureBannerlList;

    public SlidingImageAdapter(FragmentManager fm, ArrayList<ContentHome> featureBannerlList) {
        super(fm);
        this.featureBannerlList = featureBannerlList;
    }

    @Override
    public Fragment getItem(int position) {
        ScreenSlidePageFragment screenSlidePageFragment = new ScreenSlidePageFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ScreenSlidePageFragment.EXTRA_FEATURED_CONTENT, featureBannerlList.get(position));
        bundle.putSerializable(ScreenSlidePageFragment.EXTRA_FEATURED_CONTENT_LIST, featureBannerlList);
        bundle.putInt(ScreenSlidePageFragment.EXTRA_FEATURED_POSITION, position);

        screenSlidePageFragment.setArguments(bundle);

        return screenSlidePageFragment;
    }

    @Override
    public int getCount() {
        return featureBannerlList.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        //Do NOTHING;
    }
}
