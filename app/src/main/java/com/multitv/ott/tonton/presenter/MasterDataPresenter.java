package com.multitv.ott.tonton.presenter;

/**
 * Created by naseeb on 11/8/2017.
 */

public interface MasterDataPresenter {
    void getMasterData();
}
