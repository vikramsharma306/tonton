package com.multitv.ott.tonton.fragments

import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.backends.pipeline.PipelineDraweeController
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder
import com.facebook.imagepipeline.request.Postprocessor
import com.multitv.ott.tonton.R
import com.multitv.ott.tonton.adapter.CastAdapter
import com.multitv.ott.tonton.listeners.OnAlertDialogButtonClicked
import com.multitv.ott.tonton.model.master.contentdetail.ContentDetails
import com.multitv.ott.tonton.networkrequest.CommonApiListener
import com.multitv.ott.tonton.networkrequest.CommonApiPresenterImpl
import com.multitv.ott.tonton.uttils.*
import com.squareup.picasso.Picasso
import jp.wasabeef.fresco.processors.BlurPostprocessor
import kotlinx.android.synthetic.main.fragment_content_details.view.*
import java.util.*


class ContentDetailFragment : Fragment() {
    //7982410125
    private var castAdapter: CastAdapter? = null
    private val castList = ArrayList<ContentDetails.Content.Meta.AllCast>()
    private lateinit var contentId: String
    private lateinit var rootView: View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_content_details, container, false)
        val bundle = arguments
        contentId = bundle?.getString(Constant.CONTENT_ID)!!

        getContentDetailsData(contentId)

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rootView.favVideoTv.setOnClickListener {
            context?.let { it1 ->
                AppUttils.showLoginAlertDialog(it1, object : OnAlertDialogButtonClicked {
                    override fun onPositiveButtonClicked() {
                        Toast.makeText(context, "Yes clicked!", Toast.LENGTH_SHORT).show()

                    }

                    override fun onNegativeButtonClicked() {
                        Toast.makeText(context, "Skip clicked!", Toast.LENGTH_SHORT).show()
                    }
                })
            }
        }

    }

    private fun favouriteVideoRequest(tag: String) {

    }

    private fun getContentDetailsData(contentId: String) {

        val header = HashMap<String, String>()
        val params = HashMap<String, String>()
        val contentDetailUrl: String = PreferenceData.getStringAPI(
            context,
            Constant.CONTENT_DETAIL
        ).toString() + "/device/" + "android" + "/content_id/" + contentId


        CommonApiPresenterImpl(object : CommonApiListener {
            override fun onSuccess(response: String?) {
                var contentDetails =
                    Json.parse(response?.trim { it <= ' ' }, ContentDetails::class.java)

                setContentDetails(contentDetails)
            }

            override fun onError(message: String?) {
//                finish()
            }

        }).getRequest(contentDetailUrl, Constant.CONTENT_DETAIL, header)
    }

    private fun setContentDetails(contentDetails: ContentDetails) {

        if (contentDetails != null && contentDetails.result != null && contentDetails.result.content != null) {

            if (contentDetails.result.content.thumbs != null && contentDetails.result.content.thumbs.size != 0 && contentDetails.result.content.thumbs.get(
                    0
                ).thumb != null
            ) {
//                  bannerIamgeView.setImageURI(contentDetails.result.content.thumbs.get(0).thumb.medium)

                Picasso.with(context)
                    .load(contentDetails.result.content.thumbs.get(0).thumb.medium)
                    .into(rootView.mainImageView)

                val postprocessor: Postprocessor = BlurPostprocessor(context, 5)
                val imageRequest: ImageRequest =
                    ImageRequestBuilder.newBuilderWithSource(
                        Uri.parse(
                            contentDetails.result.content.thumbs.get(
                                0
                            ).thumb.medium
                        )
                    )
                        .setPostprocessor(postprocessor)
                        .build()


                var controller = Fresco.newDraweeControllerBuilder()
                    .setImageRequest(imageRequest)
                    .setOldController(rootView.bannerImageView.getController())
                    .build() as PipelineDraweeController

                rootView.bannerImageView.setController(controller)


            }


            if (contentDetails.result.content.des != null && !TextUtils.isEmpty(contentDetails.result.content.des))
                rootView.descriptionTv.setText(contentDetails.result.content.des)


            if (contentDetails.result.content.title != null && !TextUtils.isEmpty(contentDetails.result.content.title))
                rootView.contentTitleTv.setText(contentDetails.result.content.title)
            else
                rootView.contentTitleTv.setText(getString(R.string.info_not_found_msg))

            if (contentDetails.result.content.watch != null && !TextUtils.isEmpty(contentDetails.result.content.watch)) {
                rootView.viewCountTv.setText("Views : " + contentDetails.result.content.watch)
                rootView.viewCountTv.visibility = View.VISIBLE
            } else {
                rootView.viewCountTv.visibility = View.GONE
            }

            if (contentDetails.result.content.publish_date != null && !TextUtils.isEmpty(
                    contentDetails.result.content.publish_date
                )
            ) {
                rootView.dateTv.visibility = View.VISIBLE
                rootView.dateTv.setText(ScreenUtils.getFormattedDateAndTime(contentDetails.result.content.publish_date))
            } else {
                rootView.dateTv.visibility = View.GONE
            }


            if (contentDetails.result.content.duration != null && !TextUtils.isEmpty(contentDetails.result.content.duration)) {

                val splitTime: Array<String> =
                    contentDetails.result.content.duration.split(":").toTypedArray()
                val hours = splitTime[0]
                val minutes = splitTime[1]
                val seconds = splitTime[2]
                if (hours == "00") {
                    rootView.videoDurationTv.setText(
                        minutes + ":" + seconds + " " + this.getResources()
                            .getString(R.string.minute)
                    )
                } else {
                    rootView.videoDurationTv.setText(
                        hours + ":" + minutes + " " + this.getResources()
                            .getString(R.string.hours)
                    )
                }
                rootView.videoDurationTv.visibility = View.VISIBLE
                //videoDurationTv.setText(contentDetails.result.content.duration)
            } else {
                rootView.videoDurationTv.visibility = View.GONE
            }


            if (contentDetails.result.content.meta.all_cast != null && contentDetails.result.content.meta.all_cast.size != 0) {
                castList.addAll(contentDetails.result.content.meta.all_cast)
                val linearLayoutHorizentailManager = LinearLayoutManager(context)
                linearLayoutHorizentailManager.orientation = LinearLayoutManager.HORIZONTAL
                rootView.castRecyclerView.setLayoutManager(linearLayoutHorizentailManager)
                rootView.castRecyclerView.setHasFixedSize(true)
                castAdapter =
                    CastAdapter(context, castList)
                rootView.castRecyclerView.adapter = castAdapter
                castAdapter?.notifyDataSetChanged()

                rootView.castHintTv.visibility = View.VISIBLE
                rootView.castRecyclerView.visibility = View.VISIBLE
            } else {
                rootView.castHintTv.visibility = View.GONE
                rootView.castRecyclerView.visibility = View.GONE
            }


            if (contentDetails.result.content.favorite != null && !TextUtils.isEmpty(contentDetails.result.content.favorite)) {
                if (contentDetails.result.content.favorite.equals("0"))
                    rootView.favVideoTv.setImageResource(R.mipmap.fav_unselected)
                else
                    rootView.favVideoTv.setImageResource(R.mipmap.fav_selected)
            }


            rootView.shareButton.setOnClickListener {
                context?.let { it1 ->
                    AppUttils.shareIntent(
                        contentDetails.result.content.share_url,
                        it1
                    )
                }
            }


        } else {
//            finish()
        }

    }

}