package com.multitv.ott.tonton.networkrequest;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.multitv.ott.tonton.appcontroller.AppController;


import java.util.Map;
import java.util.Set;

public class CommonApiPresenterImpl implements CommonApiPresenter {
    private CommonApiListener networkResponseListener;

    public CommonApiPresenterImpl(CommonApiListener networkResponseListener) {
        this.networkResponseListener = networkResponseListener;
    }


    @Override
    public void postRequest(String url, String apiName, Map<String, String> params, Map<String, String> headers) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    Log.e("api request", apiName + "  response::::" + response);
                    networkResponseListener.onSuccess(response);
                    /*JSONObject mObj = new JSONObject(response);
                    try {
                        JSONObject newObj = mObj.getJSONObject("data");
                        String profilePicUrlFromFb = newObj.optString("url");
                        Log.e("facebook image", " helper image url::::" + profilePicUrlFromFb);
                        facebookImageResponseLister.success(profilePicUrlFromFb);
                    } catch (JSONException e) {
                        Log.e("facebook image", " helper response::::" + e.getMessage());
                        facebookImageResponseLister.error(e.getMessage());
                    }*/

                } catch (Exception e) {
                    e.printStackTrace();
                    networkResponseListener.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("api request", apiName + "  response::::" + error.getMessage());
                networkResponseListener.onError(error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                /*Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=UTF-8");
                params.put("token", ApiRequestHelper.TOEKN.trim());
                params.put("token", ApiRequestHelper.USERNAME.trim());*/

                if (headers != null && headers.size() > 0) {
                    Set<String> keySet = headers.keySet();
                    for (String key : keySet) {
                        Log.e("api request", apiName + "  header::::" + key + "   " + headers.get(key));
                    }

                }

                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                if (params != null && params.size() > 0) {
                    Set<String> keySet = params.keySet();
                    for (String key : keySet) {
                        Log.e("api request", apiName + "  params::::" + key + "   " + params.get(key));
                    }

                }

                return params;
            }
        };
        // Adding request to request queue
        AppController.Companion.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void getRequest(String url, String apiName, Map<String, String> headers) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    Log.e("api request", apiName + "  response::::" + response);
                    networkResponseListener.onSuccess(response);
                } catch (Exception e) {
                    e.printStackTrace();
                    networkResponseListener.onError(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("api request", apiName + "  response::::" + error.getMessage());
                networkResponseListener.onError(error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                if (headers != null && headers.size() > 0) {
                    Set<String> keySet = headers.keySet();
                    for (String key : keySet) {
                        Log.e("api request", apiName + "  header::::" + key + "   " + headers.get(key));
                    }
                }

                return headers;
            }

        };
        AppController.Companion.getInstance().addToRequestQueue(jsonObjReq);
        }
    }
