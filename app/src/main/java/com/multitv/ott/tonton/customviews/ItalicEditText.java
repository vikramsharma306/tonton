package com.multitv.ott.tonton.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class ItalicEditText extends AppCompatEditText {

    public ItalicEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ItalicEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ItalicEditText(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/italic.ttf");
        setTypeface(tf);
    }

}