package com.multitv.ott.tonton.fragments

import android.graphics.Matrix
import android.graphics.RectF
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.OrientationHelper
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.android.exoplayer2.util.Util
import com.google.android.exoplayer2.video.VideoListener
import com.multitv.ott.tonton.R
import com.multitv.ott.tonton.adapter.TicTocAdapter
import com.multitv.ott.tonton.appcontroller.AppController
import com.multitv.ott.tonton.customviews.OnViewPagerListener
import com.multitv.ott.tonton.customviews.ViewPagerLayoutManager
import com.multitv.ott.tonton.uttils.ImageUtils
import com.multitv.ott.tonton.uttils.ScreenUtils
import com.multitv.ott.tonton.uttils.Tracer
import dueeke.dkplayer.util.cache.PreloadManager
import kotlinx.android.synthetic.main.fragment_short_video.view.*
import java.util.*

class ShortVideosFragment : Fragment() {
    private var rootview: View? = null
    private var layoutManager: ViewPagerLayoutManager? = null
    private var mCurPos = 0

    private var homeTicTocAdapter: TicTocAdapter? = null

    private var cacheDataSourceFactory: CacheDataSourceFactory? = null
    private var simpleExoPlayer: SimpleExoPlayer? = null
    private var simpleCache: SimpleCache? = null

    private val videoCacheUrlList: ArrayList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootview = inflater.inflate(R.layout.fragment_short_video, container, false)


        initliazeView()

        return rootview
    }

    private fun initliazeView() {
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade449c6456/985_61ade449c6456_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade44b7cd91/985_61ade44b7cd91_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade44cb29b9/985_61ade44cb29b9_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade44e6b74e/985_61ade44e6b74e_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade44f3ee5f/985_61ade44f3ee5f_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade45204e46/985_61ade45204e46_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade449c6456/985_61ade449c6456_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade448d5bf5/985_61ade448d5bf5_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade447d5a3f/985_61ade447d5a3f_master.m3u8")
        videoCacheUrlList.add("http://dvaus8cgor4ds.cloudfront.net/985/985_61ade40ce7623/985_61ade40ce7623_master.m3u8")

        homeTicTocAdapter = TicTocAdapter(requireActivity(), videoCacheUrlList)
        layoutManager = ViewPagerLayoutManager(activity, OrientationHelper.VERTICAL)
        layoutManager?.initialPrefetchItemCount = 3
        rootview?.tictocRecyclerview?.setItemViewCacheSize(20)
        layoutManager?.setExtraLayoutSpace(ScreenUtils.getScreenHeight(getActivity()))
        rootview?.tictocRecyclerview?.setLayoutManager(layoutManager)


        rootview?.tictocRecyclerview?.adapter = homeTicTocAdapter
        homeTicTocAdapter!!.notifyDataSetChanged()

        layoutManager!!.setOnViewPagerListener(object : OnViewPagerListener {
            override fun onInitComplete() {
                Log.e("Recyclerview10", "onInitComplete: $mCurPos")
                setVideoPlayer(0)
            }

            override fun onPageRelease(isNext: Boolean, position: Int) {
                Log.e("Recyclerview11", "onPageRelease: $position")

                if (mCurPos == position) {
                    PreloadManager.getInstance(activity)
                        .removePreloadTask(videoCacheUrlList.get(position))
                    if (simpleExoPlayer != null)
                        simpleExoPlayer?.release()
                }

            }

            override fun onPageSelected(position: Int, isBottom: Boolean) {
                val findViewByPosition = layoutManager?.findFirstVisibleItemPosition()!!
                PreloadManager.getInstance(activity)
                    .addPreloadTask(videoCacheUrlList.get(findViewByPosition), findViewByPosition)
                if (mCurPos == layoutManager?.findFirstVisibleItemPosition()) return

                setVideoPlayer(layoutManager?.findFirstVisibleItemPosition()!!)
                Log.e("Recyclerview12", "onPageSelected: $findViewByPosition")
            }
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        if (simpleExoPlayer != null) {
            simpleExoPlayer?.release()
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        Log.e("Home Fragment===", "on setUserVisibleHint calling ::::$isVisibleToUser")
        if (isVisibleToUser) {
            Log.e("Home Fragment===", "on player calling ::::")

            if (simpleExoPlayer != null) {
                simpleExoPlayer?.playWhenReady = true
            }

        } else {
            if (simpleExoPlayer != null) {
                simpleExoPlayer?.playWhenReady = false
            }
        }
    }


    override fun onResume() {
        super.onResume()
        if (simpleExoPlayer != null) {
            simpleExoPlayer?.playWhenReady = true
        }
    }

    override fun onPause() {
        Log.e("HOme Fragment===", "on Pause calling")
        super.onPause()
        if (simpleExoPlayer != null) {
            simpleExoPlayer?.playWhenReady = false
        }
    }


    private fun setVideoPlayer(position: Int) {
        if (simpleExoPlayer != null)
            simpleExoPlayer?.release()


        rootview?.videoDataProgressBar?.visibility = View.VISIBLE

        val findViewByPosition = layoutManager!!.findViewByPosition(position)
        var playerView = findViewByPosition?.findViewById(R.id.playerView) as PlayerView
        var videoImageView = findViewByPosition?.findViewById(R.id.videoImageView) as ImageView

        simpleExoPlayer = context?.let {
            SimpleExoPlayer.Builder(it).build()
        }
        simpleCache = AppController.simpleCache

        val item = videoCacheUrlList[position]
        val playUrl = PreloadManager.getInstance(activity).getPlayUrl(item)
        Log.e("startPlay: ", "position: $position  url: $playUrl")
        val videoUri = Uri.parse(playUrl)

        /*cacheDataSourceFactory = CacheDataSourceFactory(
            simpleCache,
            DefaultHttpDataSourceFactory(context?.let {
                Util.getUserAgent(
                    it,
                    getString(R.string.app_name)
                )
            }),
            0
        )
*/
        playerView?.player = simpleExoPlayer
        simpleExoPlayer?.playWhenReady = true
        simpleExoPlayer?.seekTo(0, 0)
        simpleExoPlayer?.repeatMode = Player.REPEAT_MODE_OFF

        val mediaSource =
            HlsMediaSource.Factory(DefaultHttpDataSourceFactory(context?.let {
                Util.getUserAgent(
                    it,
                    getString(R.string.app_name)
                )
            })).createMediaSource(videoUri)

        /*  val mediaSource =
              ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(videoUri)*/

        simpleExoPlayer?.prepare(LoopingMediaSource(mediaSource), true, false)
        mCurPos = position


        simpleExoPlayer?.addVideoListener(object : VideoListener {
            override fun onVideoSizeChanged(
                width: Int,
                height: Int,
                unappliedRotationDegrees: Int,
                pixelWidthHeightRatio: Float
            ) {
                Tracer.error(
                    "Video mode ",
                    "===" + "pixelWidthHeightRatio::::" + pixelWidthHeightRatio
                )
                if (width > height) {
                    Tracer.error("Video mode ", "===" + "Landscape")
                  /*  var textureView = playerView.getVideoSurfaceView() as SurfaceView
                    ImageUtils.applyTextureViewRotation(textureView, 90)*/
                    playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT)

                } else {
                    Tracer.error("Video mode ", "===" + "Vertical")
                    playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL)
                }
            }

            override fun onRenderedFirstFrame() {}
        })


        simpleExoPlayer?.addListener(object : Player.EventListener {


            override fun onPlayerError(error: ExoPlaybackException) {
                Tracer.error("Exo Player::::", " Error::::" + error.message)
            }


            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                when (playbackState) {
                    Player.STATE_READY -> {
                        rootview?.videoDataProgressBar?.visibility = View.GONE
                        playerView?.visibility = View.VISIBLE
                        videoImageView!!.visibility = View.GONE
                        /* if (AppConstant.IsFollowFragment && !AppConstant.IsTrendingOpened)
                             simpleExoPlayer?.playWhenReady = true*/
                    }
                    Player.STATE_ENDED -> {
                        //playerCallback?.onPlayEnded()
                    }
                    Player.STATE_BUFFERING -> {
                        rootview?.videoDataProgressBar?.visibility = View.VISIBLE
                    }
                    Player.STATE_IDLE -> {

                    }
                }

                Tracer.error("Exo Player::::", " State::::" + playWhenReady)
            }

        })
    }


}