package com.multitv.ott.tonton.presenter;

/**
 * Created by naseeb on 11/8/2017.
 */

public interface MasterDataListener {
    void onMasterDataSuccess(String nType, String androidAppVersion, long timeStamp);

    void onMasterDataFail();
}
