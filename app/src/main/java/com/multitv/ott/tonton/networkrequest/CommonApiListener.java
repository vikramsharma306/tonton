package com.multitv.ott.tonton.networkrequest;

public interface CommonApiListener {
    void onSuccess(String response);

    void onError(String message);
}
