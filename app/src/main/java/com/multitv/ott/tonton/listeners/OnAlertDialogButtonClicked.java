package com.multitv.ott.tonton.listeners;

public interface OnAlertDialogButtonClicked {

    void onPositiveButtonClicked();

    void onNegativeButtonClicked();

}
