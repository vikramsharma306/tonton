package com.multitv.ott.tonton.listeners;

import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.multitv.ott.tonton.model.master.home.ContentHome;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 14-04-2017.
 */

public interface HomeCategoryAddListener {

        void setHomeCategoryDataOnLoadMoreCall(RecyclerView recyclerView, ProgressBar progressBar, int postion, String cat_id,  ArrayList<ContentHome> contentHomeList);

}