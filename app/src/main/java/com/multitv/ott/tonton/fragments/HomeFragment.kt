package com.multitv.ott.tonton.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.multitv.ott.tonton.R
import com.multitv.ott.tonton.adapter.HomeCategoryItemAdapter
import com.multitv.ott.tonton.adapter.HomeDisplayCategoryHeaderAdapter
import com.multitv.ott.tonton.adapter.LiveHomeChannelAdapter
import com.multitv.ott.tonton.adapter.SlidingImageAdapter
import com.multitv.ott.tonton.listeners.ContentDetailListener
import com.multitv.ott.tonton.listeners.HomeCategoryAddListener
import com.multitv.ott.tonton.model.master.home.ContentHome
import com.multitv.ott.tonton.model.master.home.Home
import com.multitv.ott.tonton.model.master.home.Home_category
import com.multitv.ott.tonton.model.master.live.LiveParent
import com.multitv.ott.tonton.networkrequest.CommonApiListener
import com.multitv.ott.tonton.networkrequest.CommonApiPresenterImpl
import com.multitv.ott.tonton.uttils.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment(), HomeCategoryAddListener {

    private var homeDataSaved: Home? = null
    private var liveParent: LiveParent? = null

    private var rootview: View? = null
    private val displayCategoryItemList = ArrayList<Home_category>()
    private val liveArrayList = ArrayList<ContentHome>()

    private var featureCounter = 0
    private var featureHandler: Handler? = null
    private var featureRunnable: Runnable? = null
    private var isFeatureHandlerStarted = false
    private var homeDisplayCategoryHeaderAdapter: HomeDisplayCategoryHeaderAdapter? = null
    private var liveHomeChannelAdapter: LiveHomeChannelAdapter? = null

    private var contentDetailListener: ContentDetailListener? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            contentDetailListener = context as ContentDetailListener
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement onViewSelected")
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootview = inflater.inflate(R.layout.fragment_home, container, false)


        initliazeView()

        return rootview;
    }


    private fun initliazeView() {
        val linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        rootview?.displaycategoryRecyclerview?.setLayoutManager(linearLayoutManager)
        rootview?.displaycategoryRecyclerview?.setHasFixedSize(true)

        homeDisplayCategoryHeaderAdapter =
            HomeDisplayCategoryHeaderAdapter(
                activity,
                displayCategoryItemList,
                rootview?.displaycategoryRecyclerview,
                this
            )
        rootview?.displaycategoryRecyclerview?.adapter = homeDisplayCategoryHeaderAdapter


        val linearLayoutHorizentailManager = LinearLayoutManager(activity)
        linearLayoutHorizentailManager.orientation = LinearLayoutManager.HORIZONTAL
        rootview?.liveRecyclerview?.setLayoutManager(linearLayoutHorizentailManager)
        rootview?.liveRecyclerview?.setHasFixedSize(true)
        liveHomeChannelAdapter =
            LiveHomeChannelAdapter(activity, liveArrayList, rootview?.liveRecyclerview, contentDetailListener)
        rootview?.liveRecyclerview?.adapter = liveHomeChannelAdapter


        fetchingHomeDataFromServer(false)
        fetchLiveContentData(false, 0)
    }


    private fun fetchLiveContentData(isLoadMoreRequest: Boolean, contentCount: Int) {
        if (isLoadMoreRequest) {
            rootview?.progressBarLayout?.visibility = View.GONE
            rootview?.liveLoadMoreProgressBar?.visibility = View.VISIBLE
        } else {
            rootview?.progressBarLayout?.visibility = View.VISIBLE
            rootview?.liveLoadMoreProgressBar?.visibility = View.GONE
        }

        val header = HashMap<String, String>()
        val params = HashMap<String, String>()

        var liveUrl: String = PreferenceData.getStringAPI(context, Constant.LIVE_API)
        liveUrl = liveUrl + "/device/android/" + "live_offset/" + contentCount + "/live_limit/10"


        CommonApiPresenterImpl(object : CommonApiListener {
            override fun onSuccess(response: String?) {
                rootview?.progressBarLayout?.visibility = View.GONE
                rootview?.liveLoadMoreProgressBar?.visibility = View.GONE

                liveParent = Json.parse(response?.trim { it <= ' ' }, LiveParent::class.java)

                if (liveParent != null && liveParent?.result?.live != null && liveParent?.result?.live?.size != 0)
                    liveArrayList.addAll(liveParent?.result?.live!!)

                setLiveChannelData()
            }

            override fun onError(message: String?) {
                rootview?.progressBarLayout?.visibility = View.GONE
                rootview?.liveLoadMoreProgressBar?.visibility = View.GONE

                if (!isLoadMoreRequest) {
                    rootview?.liveParentLinearLayout?.visibility = View.GONE
                }
            }

        }).getRequest(liveUrl, Constant.LIVE_API, header)
    }


    private fun setLiveChannelData() {
        if (displayCategoryItemList != null && displayCategoryItemList.size != 0) {
            liveHomeChannelAdapter?.notifyDataSetChanged()
            rootview?.liveParentLinearLayout?.visibility = View.VISIBLE
        } else {
            rootview?.liveParentLinearLayout?.visibility = View.GONE
        }

    }


    private fun fetchingHomeDataFromServer(isCategoryDataLoad: Boolean) {

        if (isCategoryDataLoad) {
            rootview?.progressBarLayout?.visibility = View.GONE
            rootview?.displayCategoryLoadMoreProgressBar?.visibility = View.VISIBLE
        } else {
            rootview?.progressBarLayout?.visibility = View.VISIBLE
            rootview?.displayCategoryLoadMoreProgressBar?.visibility = View.GONE
        }

        val header = HashMap<String, String>()
        val params = HashMap<String, String>()
        val homeUrl: String = PreferenceData.getStringAPI(context, Constant.HOME_API)
            .toString() + "/device/android/" + "display_offset/" + (if (homeDataSaved == null) 0 else homeDataSaved?.result?.display_offset) + "/display_limit/" + "3" + "/content_count/" + "10"
        CommonApiPresenterImpl(object : CommonApiListener {
            override fun onSuccess(response: String?) {
                rootview?.progressBarLayout?.visibility = View.GONE
                rootview?.displayCategoryLoadMoreProgressBar?.visibility = View.GONE

                homeDataSaved = Json.parse(response?.trim { it <= ' ' }, Home::class.java)
                if (!isCategoryDataLoad && homeDataSaved != null && homeDataSaved?.result != null && homeDataSaved?.result?.dashboard != null &&
                    homeDataSaved?.result?.dashboard?.feature_banner != null && homeDataSaved?.result?.dashboard?.feature_banner?.size != 0
                ) {
                    setFeatureBannerData(homeDataSaved?.result?.dashboard?.feature_banner!!)
                }


                if (!isCategoryDataLoad && homeDataSaved != null && homeDataSaved?.result != null && homeDataSaved?.result?.dashboard != null &&
                    homeDataSaved?.result?.dashboard?.home_category != null && homeDataSaved?.result?.dashboard?.home_category?.size != 0
                ) {
                    displayCategoryItemList.addAll(homeDataSaved?.result?.dashboard?.home_category!!)

                    setHomeCategoryData()
                }

            }

            override fun onError(message: String?) {
                rootview?.progressBarLayout?.visibility = View.GONE
                rootview?.displayCategoryLoadMoreProgressBar?.visibility = View.GONE
            }

        }).getRequest(homeUrl, Constant.HOME_API, header)
    }

    private fun setFeatureBannerData(featureBannerList: ArrayList<ContentHome>) {
        if (featureBannerList != null && featureBannerList.size != 0) {

            Tracer.error("Banner List Size::::", "" + featureBannerList.size)

            rootview?.sliderParentRelativeLayout?.visibility = View.VISIBLE
            val width: Int = ScreenUtils.getScreenWidth(activity)
            val height = width * 9 / 16
            rootview?.sliderViewPager?.setLayoutParams(RelativeLayout.LayoutParams(width, height))

            //-------------set view pager width and height run time 16:9 rasio--------------------
            val childFragmentManager: FragmentManager = childFragmentManager
            val slidingImageAdapter = SlidingImageAdapter(childFragmentManager, featureBannerList!!)
            rootview?.sliderViewPager?.setAdapter(slidingImageAdapter)
            rootview?.indicator?.setViewPager(rootview?.sliderViewPager)

            rootview?.sliderViewPager?.setCurrentItem(featureCounter)

            if (featureHandler != null && isFeatureHandlerStarted || featureBannerList.size != 1) return
            featureHandler = Handler()

            featureRunnable = object : Runnable {
                override fun run() {
                    featureCounter++
                    if (featureCounter >= featureBannerList.size) {
                        featureCounter = 0
                    }
                    rootview?.sliderViewPager?.setCurrentItem(featureCounter)
                    isFeatureHandlerStarted = featureHandler!!.postDelayed(this, 7000)

                }
            }
            isFeatureHandlerStarted = featureHandler?.postDelayed(featureRunnable!!, 7000)!!

            rootview?.sliderViewPager?.addOnPageChangeListener(object :
                ViewPager.OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                    try {
                        featureCounter = position
                    } catch (e: Exception) {
                        //Tracer.error("OUT-OF-BOUND-Exception", "FEATURE-BANNER" + e.getMessage());
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
        } else {
            Tracer.error("Banner List Size::::", "" + featureBannerList.size)
            rootview?.sliderParentRelativeLayout?.visibility = View.GONE
        }
    }


    private fun setHomeCategoryData() {
        if (displayCategoryItemList != null && displayCategoryItemList.size != 0) {
            homeDisplayCategoryHeaderAdapter?.notifyDataSetChanged()
            rootview?.displayCategoryParentLinearLayout?.visibility = View.VISIBLE
        } else {
            rootview?.displayCategoryParentLinearLayout?.visibility = View.GONE
        }

    }

    override fun setHomeCategoryDataOnLoadMoreCall(
        recyclerView: RecyclerView?,
        progressBar: ProgressBar?,
        postion: Int,
        cat_id: String?,
        contentHomeList: java.util.ArrayList<ContentHome>?
    ) {


        val linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        recyclerView!!.layoutManager = linearLayoutManager
        recyclerView!!.setHasFixedSize(true)
        val offset: String
        offset = if (contentHomeList != null && contentHomeList.size > 0) {
            "" + contentHomeList.size
        } else {
            "5"
        }

        val homeDisplayItemWithHeaderAdapter = HomeCategoryItemAdapter(
            activity,
            contentHomeList,
            recyclerView,
            offset,
            "5",
            cat_id,
            contentDetailListener
        )
        recyclerView!!.swapAdapter(homeDisplayItemWithHeaderAdapter, true)
        recyclerView!!.isNestedScrollingEnabled = false
        homeDisplayItemWithHeaderAdapter.notifyDataSetChanged()
    }

}