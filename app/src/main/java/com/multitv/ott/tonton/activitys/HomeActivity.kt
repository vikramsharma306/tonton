package com.multitv.ott.tonton.activitys

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.multitv.ott.tonton.R
import com.multitv.ott.tonton.fragments.ContentDetailFragment
import com.multitv.ott.tonton.fragments.HomeFragment
import com.multitv.ott.tonton.fragments.ShortVideosFragment
import com.multitv.ott.tonton.listeners.ContentDetailListener
import com.multitv.ott.tonton.uttils.Constant
import kotlinx.android.synthetic.main.activity_main.*

class HomeActivity : AppCompatActivity(), ContentDetailListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fragmentTranscation(HomeFragment())


        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_home -> {
                    toolbar.visibility = View.VISIBLE
                    fragmentTranscation(HomeFragment())
                    true
                }
                R.id.navigation_short_video -> {
                    toolbar.visibility = View.GONE
                    fragmentTranscation(ShortVideosFragment())
                    true
                }
                else -> false
            }
        }
    }

    private fun fragmentTranscation(fragment: Fragment) {
        var bundle = Bundle()
        bundle.putBoolean("values", true)
        fragment.setArguments(bundle)
        var ft = getSupportFragmentManager().beginTransaction()
        ft.replace(R.id.frame, fragment).addToBackStack(null);
        ft.commitAllowingStateLoss()
    }


    override fun onContentClick(id: String?) {
        var fragment = ContentDetailFragment()
        var bundle = Bundle()
        bundle.putString(Constant.CONTENT_ID, id)
        fragment.setArguments(bundle)
        var ft = getSupportFragmentManager().beginTransaction()
        ft.replace(R.id.frame, fragment).addToBackStack(null);
        ft.commitAllowingStateLoss()
    }

}