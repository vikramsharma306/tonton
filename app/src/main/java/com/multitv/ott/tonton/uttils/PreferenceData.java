package com.multitv.ott.tonton.uttils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.multitv.ott.tonton.apirequest.ApiRequestHelper;

import java.util.Map;
import java.util.Set;

public class PreferenceData {

    private static final String TAG = "BASE_TAG" + ".PreferenceData";
    private static final String STORE = "STORE";
    private static final String IS_HOME_FLOAT_DEMO_DONE = "IS_HOME_FLOAT_DEMO_DONE";
    private static final String IS_TC_DONE = "IS_TC_DONE";
    private static final String IS_UPDATE_TOKEN = "IS_UPDATE_TOKEN";
    private static final String LANGUAGE_LIST = "LANGUAGE_LIST";
    private static final String FCM_TOKEN = "FCM_TOKEN";
    private static final String APP_SESSION_ID = "APP_SESSION_ID";
    private static final String APP_LANGUAGE = "APP_LANGUAGE";
    private static final String IS_FCM_TOKEN_REGISTER_ON_SERVER = "IS_FCM_TOKEN_REGISTER_ON_SERVER";
    private static final String FLOAT_X = "FLOAT_X";
    private static final String FLOAT_Y = "FLOAT_Y";
    private static final String NOTIFICATION_COUNTER = "NOTIFICATION_COUNTER";
    private static final String APP_STATE = "APP_STATE";
    private static final String IS_NOTIFICATION_ENABLE = "IS_NOTIFICATION_ENABLE";
    private static final String IS_MATURE_FILTER_ENABLE = "IS_MATURE_FILTER_ENABLE";
    private static final String PREVIOUSLY_SELECTED_FRAGMENT_POSITION = "PREVIOUSLY_SELECTED_FRAGMENT_POSITION";
    private static final String VERSION_DATA = "VERSION_DATA";
    private static final String POST_URL_LOW_CACHE = "POST_URL_LOW_CACHE";
    private static final String POST_URL_MEDIUM_CACHE = "POST_URL_MEDIUM_CACHE";
    private static final String POST_URL_HIGH_CACHE = "POST_URL_HIGH_CACHE";
    private static final String GET_URL_LOW_CACHE = "GET_URL_LOW_CACHE";
    private static final String GET_URL_MEDIUM_CACHE = "GET_URL_LOW_CACHE";
    private static final String GET_URL_HIGH_CACHE = "GET_URL_LOW_CACHE";
    private static final String SUBSCRIPTION_URL = "SUBSCRIPTION_URL";
    private static final String ANALYTICS_URL = "ANALYTICS_URL";
    private static final String ACKNOWLEDGEMENT_URL = "ACKNOWLEDGEMENT_URL";

    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    public static void setVersionData(Context context, String versionData, String appVersion) {
        Tracer.debug(TAG, "setVersionData: ");
        getShearedPreferenceEditor(context).putString(VERSION_DATA + "_" + appVersion, versionData).commit();
    }

    public static String getVersionData(Context context, String appVersion) {
        Tracer.debug(TAG, "getVersionData: ");
        return getSharedPreference(context).getString(VERSION_DATA + "_" + appVersion, "");
    }

    /**
     * Method to notify that the FCM Token is registered on the server
     *
     * @param context
     * @return
     */
    public static void setBasePostGetSubscriptionUrl(Context context,
                                                     String postUrlLowCache, String postUrlMediumCache, String postUrlHighCache,
                                                     String getUrlLowCache, String getUrlMediumCache, String getUrlHighCache,
                                                     String subscriptionUrl, String analyticsUrl, String acknowledgementUrl) {
        Tracer.debug(TAG, "setBasePostGetSubscriptionUrl: ");
        if (!TextUtils.isEmpty(postUrlLowCache))
            getShearedPreferenceEditor(context).putString(POST_URL_LOW_CACHE, postUrlLowCache).commit();
        if (!TextUtils.isEmpty(postUrlMediumCache))
            getShearedPreferenceEditor(context).putString(POST_URL_MEDIUM_CACHE, postUrlMediumCache).commit();
        if (!TextUtils.isEmpty(postUrlHighCache))
            getShearedPreferenceEditor(context).putString(POST_URL_HIGH_CACHE, postUrlHighCache).commit();
        if (!TextUtils.isEmpty(getUrlLowCache))
            getShearedPreferenceEditor(context).putString(GET_URL_LOW_CACHE, getUrlLowCache).commit();
        if (!TextUtils.isEmpty(getUrlMediumCache))
            getShearedPreferenceEditor(context).putString(GET_URL_MEDIUM_CACHE, getUrlMediumCache).commit();
        if (!TextUtils.isEmpty(getUrlHighCache))
            getShearedPreferenceEditor(context).putString(GET_URL_HIGH_CACHE, getUrlHighCache).commit();
        if (!TextUtils.isEmpty(subscriptionUrl))
            getShearedPreferenceEditor(context).putString(SUBSCRIPTION_URL, subscriptionUrl).commit();
        if (!TextUtils.isEmpty(analyticsUrl))
            getShearedPreferenceEditor(context).putString(ANALYTICS_URL, analyticsUrl).commit();
        if (!TextUtils.isEmpty(acknowledgementUrl))
            getShearedPreferenceEditor(context).putString(ACKNOWLEDGEMENT_URL, acknowledgementUrl).commit();
    }

    /**
     * Method to fetch base url from shared preference
     *
     * @param context
     * @return
     */
    public static String getBaseUrl(Context context) {
        Tracer.debug(TAG, "getBaseUrl: ");
        return getSharedPreference(context).getString(POST_URL_HIGH_CACHE, "");
    }

    public static String getGetUrl(Context context) {
        Tracer.debug(TAG, "getGetUrl: ");
        return getSharedPreference(context).getString(GET_URL_HIGH_CACHE, "");
    }

    public static String getSubscriptionUrl(Context context) {
        Tracer.debug(TAG, "getSubscriptionUrl: ");
        return getSharedPreference(context).getString(SUBSCRIPTION_URL, "");
    }

    public static String getAnalyticsUrl(Context context) {
        Tracer.debug(TAG, "getAnalyticsUrl: ");
        return getSharedPreference(context).getString(ANALYTICS_URL, "");
    }

    public static String generateUrl(String apiUrl, Map<String, String> params) {
        String paramsUrl = "";
        Set<String> keys = params.keySet();
        for (String key : keys) {
            paramsUrl = paramsUrl + "/" + key + "/" + params.get(key);
        }
        return apiUrl + paramsUrl;
        /*  return PreferenceData.getGetUrl(AppController.getInstance()) + apiUrl + paramsUrl;*/
    }


    public static String getStringAPI(Context context, String key) {
        if (key == null)
            return null;

        SharedPreferences sharedPreference = context.getSharedPreferences(Constant.PREFS_NAME, Context.MODE_PRIVATE);
        return sharedPreference.getString(key, null) + ApiRequestHelper.TOKEN;
    }


    public static String getStringAPIWithoutToken(Context context, String key) {
        try {
            if (key == null)
                return null;

            SharedPreferences sharedPreference = context.getSharedPreferences(Constant.PREFS_NAME, Context.MODE_PRIVATE);
            return sharedPreference.getString(key, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getEncState(Context context, String key) {
        try {
            if (key == null) {
                return "";
            }
            SharedPreferences sharedPreference = context.getSharedPreferences(Constant.PREFS_NAME, Context.MODE_PRIVATE);
            String state = sharedPreference.getString(key, null);
            if (state != null) {
                return state;
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Method to check weather the Float button demo is shown to user
     *
     * @param context
     * @return
     */
    public static boolean isHomeFloatDemoDone(Context context) {
        // Tracer.error(TAG, "isHomeFloatDemoDone: ");
        return getSharedPreference(context).getBoolean(IS_HOME_FLOAT_DEMO_DONE, false);
    }

    /**
     * Method to notify that Float button demo is shown to user
     *
     * @param context
     * @return
     */
    public static void setHomeFloatDemoDone(Context context) {
        Tracer.error(TAG, "setHomeFloatDemoDone: ");
        getShearedPreferenceEditor(context).putBoolean(IS_HOME_FLOAT_DEMO_DONE, true).commit();
    }

    /**
     * Method to check weather the App is active or not
     *
     * @param context
     * @return
     */
    public static boolean isAppActive(Context context) {
        //Tracer.error(TAG, "isAppActive: ");
        return getSharedPreference(context).getBoolean(APP_STATE, true);
    }

    /**
     * Method to notify that user set that App is active
     *
     * @param context
     * @return
     */
    public static void setAppActive(Context context) {
        // Tracer.error(TAG, "setAppActive: ");
        getShearedPreferenceEditor(context).putBoolean(APP_STATE, true).commit();
    }


    /**
     * Method to notify that user set that App is inactive
     *
     * @param context
     * @return
     */
    public static void setAppInactive(Context context) {
        Tracer.error(TAG, "setAppInactive: ");
        getShearedPreferenceEditor(context).putBoolean(APP_STATE, false).commit();
    }

    /**
     * Method to check weather the TC is shown ornot
     *
     * @param context
     * @return
     */
    public static boolean isTCShown(Context context) {
        Tracer.error(TAG, "isTCShown: ");
        return getSharedPreference(context).getBoolean(IS_TC_DONE, false);
    }

    /**
     * Method to notify that TC Shown to user
     *
     * @param context
     * @return
     */
    public static void setTCShown(Context context) {
        Tracer.error(TAG, "setTCShown: ");
        getShearedPreferenceEditor(context).putBoolean(IS_TC_DONE, true).commit();
    }

    /**
     * Method to get the supported language List
     *
     * @param context
     * @return
     */
    public static String getLangaugeListJsonArray(Context context) {
        Tracer.error(TAG, "getLangaugeListJsonArray: ");
        return getSharedPreference(context).getString(LANGUAGE_LIST, "[]");
    }

    /**
     * Method to set the supported language List
     *
     * @param context
     * @param languageList
     * @return
     */
    public static void setLangaugeListJsonArray(Context context, String languageList) {
        Tracer.error(TAG, "setLangaugeListJsonArray: ");
        getShearedPreferenceEditor(context).putString(LANGUAGE_LIST, languageList).commit();
    }

    /**
     * Method to get the Float Button X
     *
     * @param context
     * @return
     */
    public static int getFloatX(Context context) {
        Tracer.error(TAG, "getFloatX: ");
        return getSharedPreference(context).getInt(FLOAT_X, -1);
    }

    /**
     * Method to set the Float Button X
     *
     * @param context
     * @param floatX
     * @return
     */
    public static void setFloatX(Context context, int floatX) {
        Tracer.error(TAG, "setFloatX: ");
        getShearedPreferenceEditor(context).putInt(FLOAT_X, floatX).commit();
    }


    /**
     * Method to get the Float Button Y
     *
     * @param context
     * @return
     */
    public static int getFloatY(Context context) {
        Tracer.error(TAG, "getFloatY: ");
        return getSharedPreference(context).getInt(FLOAT_Y, -1);
    }

    /**
     * Method to set the Float Button Y
     *
     * @param context
     * @param floatY
     * @return
     */
    public static void setFloatY(Context context, int floatY) {
        Tracer.error(TAG, "setFloatY: ");
        getShearedPreferenceEditor(context).putInt(FLOAT_Y, floatY).commit();
    }

    /**
     * Method to get the FCM TOKEN
     *
     * @param context
     * @return
     */
    public static String getFCMToken(Context context) {
        Tracer.error(TAG, "getFCMToken: ");
        return getSharedPreference(context).getString(FCM_TOKEN, "");
    }

    /**
     * Method to set the New Fcm Token
     *
     * @param context
     * @param fcmToken
     * @return
     */
    public static void setFCMToken(Context context, String fcmToken) {
        Tracer.error(TAG, "setFCMToken: ");
        getShearedPreferenceEditor(context).putString(FCM_TOKEN, fcmToken).commit();
        setNewFCMTokenGenerated(context);
    }

    /**
     * Method to check weather the FCM Token is regictered on the server or not
     *
     * @param context
     * @return
     */
    public static boolean isFCMTokenRegisteredOnServer(Context context) {
        Tracer.error(TAG, "isFCMTokenRegisteredOnServer: ");
        return getSharedPreference(context).getBoolean(IS_FCM_TOKEN_REGISTER_ON_SERVER, false);
    }

    /**
     * Method to notify that the FCM Token is registered on the server
     *
     * @param context
     * @return
     */
    public static void setFCMTokenRegisteredOnServer(Context context) {
        Tracer.error(TAG, "setFCMTokenRegisteredOnServer: ");
        getShearedPreferenceEditor(context).putBoolean(IS_FCM_TOKEN_REGISTER_ON_SERVER, true).commit();
    }

    /**
     * Method to notify that the New FCM Token is Generated
     *
     * @param context
     * @return
     */
    public static void setNewFCMTokenGenerated(Context context) {
        Tracer.error(TAG, "setNewFCMTokenGenerated: ");
        getShearedPreferenceEditor(context).putBoolean(IS_FCM_TOKEN_REGISTER_ON_SERVER, false).commit();
    }

    /**
     * method to check is the update token query is run or not
     *
     * @param context
     * @return
     */
    public static boolean isAlreadyUpdateTokenDone(Context context) {
        return getSharedPreference(context).getBoolean(IS_UPDATE_TOKEN, false);
    }

    /**
     * Method to set the Update token query is run already
     *
     * @param context
     * @return
     */
    public static void setAlreadyUpdateTokenDone(Context context) {
        getShearedPreferenceEditor(context).putBoolean(IS_UPDATE_TOKEN, true).commit();
    }

    /**
     * Method to get the AppSessionId
     *
     * @param context
     * @return
     */
    public static String getAppSessionId(Context context) {
        //Tracer.error(TAG, "getAppSessionId: ");
        return getSharedPreference(context).getString(APP_SESSION_ID, "");
    }

    /**
     * Method to set the New AppSessionId
     *
     * @param context
     * @param appSessionId
     * @return
     */
    public static void setAppSessionId(Context context, String appSessionId) {
        //Tracer.error(TAG, "setAppSessionId: ");
        getShearedPreferenceEditor(context).putString(APP_SESSION_ID, appSessionId).commit();
        setNewFCMTokenGenerated(context);
    }

    /**
     * Method to get the Unique Notification Id
     *
     * @param context
     * @return
     */
    public static int getNotificationId(Context context) {
        //Tracer.error(TAG, "getNotificationId: ");
        incrementNotificationId(context);
        return getSharedPreference(context).getInt(NOTIFICATION_COUNTER, 0);
    }

    /**
     * Method to inc the Notification Id
     *
     * @param context
     * @return
     */
    public static void incrementNotificationId(Context context) {
        //Tracer.error(TAG, "incrementNotificationId: ");
        int notificationId = getSharedPreference(context).getInt(NOTIFICATION_COUNTER, 0);
        getShearedPreferenceEditor(context).putInt(NOTIFICATION_COUNTER, notificationId + 1).commit();
    }

    /**
     * Method to check weather the Notification is enable or not
     *
     * @param context
     * @return
     */
    public static boolean isNotificationEnable(Context context) {
        //Tracer.error(TAG, "isNotificationEnable: ");
        return getSharedPreference(context).getBoolean(IS_NOTIFICATION_ENABLE, true);
    }

    /**
     * Method to notify that user toggle the Notification
     *
     * @param context
     * @return
     */
    public static void toggleNotification(Context context) {
        //Tracer.error(TAG, "toggleNotification: ");
        if (isNotificationEnable(context)) {
            disableNotifiation(context);
        } else {
            enableNotifiation(context);
        }
    }

    /**
     * Method to notify that user disable the notification
     *
     * @param context
     * @return
     */
    private static void disableNotifiation(Context context) {
        //Tracer.error(TAG, "disableNotifiation: ");
        getShearedPreferenceEditor(context).putBoolean(IS_NOTIFICATION_ENABLE, false).commit();
    }

    /**
     * Method to notify that user enable the notification
     *
     * @param context
     * @return
     */
    private static void enableNotifiation(Context context) {
        //Tracer.error(TAG, "enableNotifiation: ");
        getShearedPreferenceEditor(context).putBoolean(IS_NOTIFICATION_ENABLE, true).commit();
    }

    /**
     * Method to check weather the MatureFilter is enable or not
     *
     * @param context
     * @return
     */
    public static boolean isMatureFilterEnable(Context context) {
        //Tracer.error(TAG, "isMatureFilterEnable: ");
        return getSharedPreference(context).getBoolean(IS_MATURE_FILTER_ENABLE, true);
    }

    /**
     * Method to notify that user disable the MatureFilter
     *
     * @param context
     * @return
     */
    public static void disableMatureFilter(Context context) {
        //Tracer.error(TAG, "disableMatureFilter: ");
        getShearedPreferenceEditor(context).putBoolean(IS_MATURE_FILTER_ENABLE, false).commit();
    }

    /**
     * Method to notify that user enable the MatureFilter
     *
     * @param context
     * @return
     */
    public static void enableMatureFilter(Context context) {
        Tracer.error(TAG, "enableMatureFilter: ");
        getShearedPreferenceEditor(context).putBoolean(IS_MATURE_FILTER_ENABLE, true).commit();
    }

    /**
     * Method to get the App Language
     *
     * @param context
     * @return
     */
    public static String getAppLanguage(Context context) {
        Tracer.error(TAG, "getAppLanguage: ");
        return getSharedPreference(context).getString(APP_LANGUAGE, "");
    }

    /**
     * Method to set the New App Language
     *
     * @param context
     * @param newAppLanguage
     * @return
     */
    public static void setAppLanguage(Context context, String newAppLanguage) {
        Tracer.error(TAG, "setAppLanguage: ");
        getShearedPreferenceEditor(context).putString(APP_LANGUAGE, newAppLanguage).commit();
    }


    /**
     * Method to get position of previous fragment
     *
     * @param context
     * @return
     */
    public static int getPreviousFragmentSelectedPosition(Context context) {
        Tracer.error(TAG, "getPreviousFragmentSelectedPosition: ");
        return getSharedPreference(context).getInt(PREVIOUSLY_SELECTED_FRAGMENT_POSITION, 0);
    }

    /**
     * Method to set position of previous fragment
     *
     * @param context
     * @param previousPosition
     * @return
     */
    public static void setPreviousFragmentSelectedPosition(Context context, int previousPosition) {
        Tracer.error(TAG, "setPreviousFragmentSelectedPosition: ");
        getShearedPreferenceEditor(context).putInt(PREVIOUSLY_SELECTED_FRAGMENT_POSITION, previousPosition).commit();
    }

    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    /**
     * Method to clear the Data Store
     *
     * @param context
     */
    public static void clearStore(Context context) {
        Tracer.error(TAG, "clearStore()");
        getShearedPreferenceEditor(context).clear().commit();
    }

    /**
     * Method to return the Data Store Prefference
     *
     * @param context
     * @return
     */
    private static SharedPreferences getSharedPreference(Context context) {
        return context.getSharedPreferences(STORE, Context.MODE_PRIVATE);
    }

    /**
     * caller to commit this editor
     *
     * @param context
     * @return Editor
     */
    private static Editor getShearedPreferenceEditor(Context context) {
        return getSharedPreference(context).edit();
    }
}
