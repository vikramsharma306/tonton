
package com.multitv.ott.tonton.model.master.home;


import java.io.Serializable;

public class Meta implements Serializable {

    public String star_cast;
    public String director;
    public String music_director;
    public String producer;
    public String year;
    public String rated;
    public String released;
    public String runtime;
    public String genre;
    public String writer;
    public String plot;
    public String language;
    public String country;
    public String award;
    public String poster;
    public String metascore;
    public String imdb_rating;
    public String imdb_vote;
    public String imbd_id;
    public String type;
    public boolean drm;
    public String episodnumber;
}
