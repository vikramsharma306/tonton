package com.multitv.ott.tonton.model.master.contentdetail;

import java.util.List;

public class ContentDetails {
    public Integer code;
    public Result result;

    public class Result {
        public Content content;
    }

    public class Content {
        public String id;
        public String title;
        public String des;
        public String languageId;
        public String language;
        public String mediaType;
        public String source;
        public String duration;
        public String likesCount;
        public String dislikeCount;
        public Integer rating;
        public String watch;
        public String favoriteCount;
        public String downloadExpiry;
        public String share_url;
        public String matureContent;
        public String created;
        public String publish_date;
        public String publishEndDate;
        public Integer isPublished;
        public String contentMode;
        public String socialView;
        public String socialLike;
        public String sourceId;
        public String url;
        public String downloadPath;
        public String categories;
        public String likes;
        public String favorite;
        public List<Thumb> thumbs = null;
        public String episodeNumber;
        public String secureurl;
        public String trailerUrl;
        public String accessType;
        public String adUrl;
        public Integer resoluType;
        public String isGroup;
        public String seasonId;
        public Integer skipVideo;
        public Meta meta;

        public class Meta {

            public String drm;
            public String episodnumber;
            public String musicDirector;
            public String producer;
            public String production;
            public String year;
            public String rated;
            public String released;
            public String runtime;
            public String genre;
            public String plot;
            public String language;
            public String country;
            public String award;
            public String poster;
            public String metascore;
            public String imdbRating;
            public String imdbVote;
            public String imbdId;
            public String type;
            public String starCast;
            public String director;
            public String writer;
            public String genres;
            public List<AllCast> all_cast = null;

            public class AllCast {

                public String image;
                public String name;
                public String type;

                public AllCast(String image, String name, String type) {
                    this.image = image;
                    this.name = name;
                    this.type = type;
                }

                public String getImage() {
                    return image;
                }

                public String getName() {
                    return name;
                }

                public String getType() {
                    return type;
                }
            }


        }


        public class Thumb {
            public String name;
            public Thumb__1 thumb;

        }

        public class Thumb__1 {
            public String large;
            public String medium;
            public String small;

        }

    }
}