package com.multitv.ott.tonton.uttils;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.Log;
import android.view.TextureView;

import com.multitv.ott.tonton.model.master.home.ContentHome;


/**
 * Created by naseeb on 6/15/2017.
 */

public class ImageUtils {
    public static String getFinalImageUrlBanner(ContentHome contentHome) {
        try {
            String url = "";
            if (contentHome != null && contentHome.thumbs != null && !contentHome.thumbs.isEmpty()) {
                ContentHome.Thumb thumb = contentHome.thumbs.get(0);
                if (thumb != null && thumb.thumb != null && !TextUtils.isEmpty(thumb.thumb.medium))
                    url = thumb.thumb.medium;
            }

            if (!TextUtils.isEmpty(url)) {
                int index = url.lastIndexOf('.');
                if (index >= 0 && index < url.length()) {
                    String imageUrl = url.substring(0, index) + "_640x360" + url.substring(index);
                    return imageUrl;
                }
            }

            /*return contentHome != null && contentHome.thumb_url != null
                    && !TextUtils.isEmpty(contentHome.thumb_url.base_path)
                    && !TextUtils.isEmpty(contentHome.thumb_url.thumb_path) ?
                    getBannerImageUrl(contentHome.thumb_url.base_path,
                            contentHome.thumb_url.thumb_path) : "";*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getFinalImageUrlThumbnail(ContentHome contentHome) {
        try {
            String url = "";
            if (contentHome != null && contentHome.thumbs != null && !contentHome.thumbs.isEmpty()) {
                ContentHome.Thumb thumb = contentHome.thumbs.get(0);
                if (thumb != null && thumb.thumb != null && !TextUtils.isEmpty(thumb.thumb.medium))
                    url = thumb.thumb.medium;
            }

            if (!TextUtils.isEmpty(url) && !url.contains("_480x270")) {
                int index = url.lastIndexOf('.');
                if (index >= 0 && index < url.length()) {
                    //String imageUrl = url.substring(0, index) + "_480x270" + url.substring(index);
                    String imageUrl = url.substring(0, index) + url.substring(index);

                    return imageUrl;
                }
            } else
                return url;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getFinalImageUrlThumbnailLive(ContentHome contentHome) {
        try {
            String url = "";
            if (contentHome != null && contentHome.thumbs != null) {
                if (!TextUtils.isEmpty(contentHome.thumbs.get(0).thumb.medium))
                    url = contentHome.thumbs.get(0).thumb.medium;
            }

            if (!TextUtils.isEmpty(url)) {
                int index = url.lastIndexOf('.');
                if (index >= 0 && index < url.length()) {
                    // String imageUrl = url.substring(0, index) + "_480x270" + url.substring(index);
                    String imageUrl = url.substring(0, index) + url.substring(index);

                    return imageUrl;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getFinalImageUrlThumbnailLive2(ContentHome contentHome) {
        try {
            String url = "";
            if (contentHome != null && contentHome.thumbnail != null) {
                if (!TextUtils.isEmpty(contentHome.thumbnail.medium))
                    url = contentHome.thumbnail.medium;
            }

            if (!TextUtils.isEmpty(url)) {
                int index = url.lastIndexOf('.');
                if (index >= 0 && index < url.length()) {
                    //return url.substring(0, index) + "_640x360" + url.substring(index);
                    return url.substring(0, index) + url.substring(index);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static String getFinalImageUrlThumbnailLive1(ContentHome contentHome) {
        try {
            String url = "";
            if (contentHome != null && contentHome.thumbnail != null) {
                if (!TextUtils.isEmpty(contentHome.thumbnail.medium))
                    url = contentHome.thumbnail.medium;
            }

            if (!TextUtils.isEmpty(url)) {
                int index = url.lastIndexOf('.');
                if (index >= 0 && index < url.length()) {
                    Log.e("LIVE IMAGE", ">>>>URL>>>" + url.substring(0, index) + url.substring(index));
                    //return url.substring(0, index) + "_480x270" + url.substring(index);//----live production
                    return url.substring(0, index) + url.substring(index);//----live statging
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    private static String getBannerImageUrl(String basePath, String thumbPath) {
        try {
            if (TextUtils.isEmpty(basePath) || TextUtils.isEmpty(thumbPath))
                return null;
            return new StringBuilder().append(basePath)
                    .append("c_scale")
                    .append(",")
                    .append(Constant.BANNER_HEIGHT)
                    .append(",")
                    .append(Constant.BANNER_QUALITY)
                    .append(",")
                    .append(Constant.BANNER_WIDTH)
                    .append("/")
                    .append(thumbPath).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String getThumbnailImageUrl(String basePath, String thumbPath) {
        try {
            if (TextUtils.isEmpty(basePath) || TextUtils.isEmpty(thumbPath))
                return null;
            return new StringBuilder().append(basePath)
                    .append("c_scale")
                    .append(",")
                    .append(Constant.THUMBNAIL_HEIGHT)
                    .append(",")
                    .append(Constant.THUMBNAIL_QUALITY)
                    .append(",")
                    .append(Constant.THUMBNAIL_WIDTH)
                    .append("/")
                    .append(thumbPath).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    public static void applyTextureViewRotation(TextureView textureView, int textureViewRotation) {
        float textureViewWidth = textureView.getWidth();
        float textureViewHeight = textureView.getHeight();
        if (textureViewWidth == 0 || textureViewHeight == 0 || textureViewRotation == 0) {
            textureView.setTransform(null);
        } else {
            Matrix transformMatrix = new Matrix();
            float pivotX = textureViewWidth / 2;
            float pivotY = textureViewHeight / 2;
            transformMatrix.postRotate(textureViewRotation, pivotX, pivotY);

            // After rotation, scale the rotated texture to fit the TextureView size.
            RectF originalTextureRect = new RectF(0, 0, textureViewWidth, textureViewHeight);
            RectF rotatedTextureRect = new RectF();
            transformMatrix.mapRect(rotatedTextureRect, originalTextureRect);
            transformMatrix.postScale(
                    textureViewWidth / rotatedTextureRect.width(),
                    textureViewHeight / rotatedTextureRect.height(),
                    pivotX,
                    pivotY);
            textureView.setTransform(transformMatrix);
        }
    }

}
