package com.multitv.ott.tonton.uttils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by naseeb on 10/6/2016.
 */

public class ScreenUtils {
    private static final int REQUEST_WRITE_PERMISSION = 786;


    public static int getScreenWidth(Context context) {
        int screenWidth = 0;

        try {
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return screenWidth;
    }

    public static int getScreenHeight(Context context) {
        int screenHeight = 0;

        try {
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return screenHeight;
    }

    public static String getFormattedDateAndTime(String dateToFormat) {

        try {
            //To get day of month with th, rd, st
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat dayFormat = new SimpleDateFormat("dd");
            Date inputDateObj = inputFormat.parse(dateToFormat);
            String date = dayFormat.format(inputDateObj);
            DateFormat outputFormat;

            if (date.endsWith("1") && !date.endsWith("11"))
                outputFormat = new SimpleDateFormat("dd'st' MMM yyyy");
            else if (date.endsWith("2") && !date.endsWith("12"))
                outputFormat = new SimpleDateFormat("dd'nd' MMM yyyy");
            else if (date.endsWith("3") && !date.endsWith("13"))
                outputFormat = new SimpleDateFormat("dd'rd' MMM yyyy");
            else
                outputFormat = new SimpleDateFormat("dd'th' MMM yyyy");

            if (!TextUtils.isEmpty(dateToFormat) && outputFormat != null) {
                Date dateObj = inputFormat.parse(dateToFormat);
                return outputFormat.format(dateObj);
            }
            return "";
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "";
    }



    public static String getMobileNumber(String mobileNumber) {
        if (mobileNumber.startsWith("0")) {
            mobileNumber.replaceFirst("(?:0)+", "");
        } else if (mobileNumber.startsWith("+91")) {
            mobileNumber.replaceFirst("(?:+91)+", "");
        }else if (mobileNumber.startsWith("91")) {
            mobileNumber.replaceFirst("(?:91)+", "");
        }
        return mobileNumber;
    }


        public static byte[] convertVideoToBytes(Context context, Uri uri) {
            byte[] videoBytes = null;
            try {//  w  w w  . j ava 2s . c  o m
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                FileInputStream fis = new FileInputStream(new File(String.valueOf(uri)));
                byte[] buf = new byte[1024];
                int n;
                while (-1 != (n = fis.read(buf)))
                    baos.write(buf, 0, n);

                videoBytes = baos.toByteArray();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return videoBytes;
        }


    public static void hideKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    //-----grant permission request-----------
    public static void requestPermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        }

    }
}
