package com.multitv.ott.tonton.model.master.cast;

public class CastModel {

    private int castThumbnail;
    private String name;
    private String details;


    public CastModel(String name, String details, int castThumbnail) {
        this.name = name;
        this.castThumbnail = castThumbnail;
        this.details = details;
    }

    public int getCastThumbnail() {
        return castThumbnail;
    }

    public void setCastThumbnail(int castThumbnail) {
        this.castThumbnail = castThumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
