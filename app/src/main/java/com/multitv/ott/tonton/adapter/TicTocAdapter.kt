package com.multitv.ott.tonton.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.multitv.ott.tonton.R
import com.multitv.ott.tonton.viewholder.TicTocViewHolder
import dueeke.dkplayer.util.cache.PreloadManager

class TicTocAdapter(private val context: Context, private val videoCacheUrlList: List<String>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): TicTocViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_tictoc, viewGroup, false)
        return TicTocViewHolder(v)
    }

    override fun getItemCount(): Int {
        return videoCacheUrlList.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        (viewHolder as TicTocViewHolder).onBind(context, videoCacheUrlList.get(position), position)
    }
}