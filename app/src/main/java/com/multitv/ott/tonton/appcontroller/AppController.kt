package com.multitv.ott.tonton.appcontroller

import android.text.TextUtils
import android.util.Log
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.dueeeke.videoplayer.exo.ExoMediaPlayerFactory
import com.dueeeke.videoplayer.player.VideoViewConfig
import com.dueeeke.videoplayer.player.VideoViewManager
import com.facebook.drawee.backends.pipeline.Fresco
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.multitv.ott.tonton.database.SharedPreference
import com.multitv.ott.tonton.uttils.NukeSSLCerts

class AppController : MultiDexApplication() {
    private var mRequestQueue: RequestQueue? = null

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        Fresco.initialize(this)
        instance = this
        NukeSSLCerts().nuke()
        if (sharedPreference == null) sharedPreference = SharedPreference()


        //播放器配置，注意：此为全局配置，按需开启
        VideoViewManager.setConfig(
            VideoViewConfig.newBuilder()
            .setLogEnabled(true) //调试的时候请打开日志，方便排错
            .setPlayerFactory(ExoMediaPlayerFactory.create()) //                .setPlayerFactory(ExoMediaPlayerFactory.create())
            //                .setRenderViewFactory(SurfaceRenderViewFactory.create())
            //                .setEnableOrientation(true)
            //                .setEnableAudioFocus(false)
            //                .setScreenScaleType(VideoView.SCREEN_SCALE_MATCH_PARENT)
            //                .setAdaptCutout(false)
            //                .setPlayOnMobileNetwork(true)
            //                .setProgressManager(new ProgressManagerImpl())
            .build())

        if (leastRecentlyUsedCacheEvictor == null) {
            leastRecentlyUsedCacheEvictor = LeastRecentlyUsedCacheEvictor(exoPlayerCacheSize)
        }

        if (exoDatabaseProvider != null) {
            exoDatabaseProvider = ExoDatabaseProvider(this)
        }

        if (simpleCache == null) {
            simpleCache = SimpleCache(cacheDir, leastRecentlyUsedCacheEvictor, exoDatabaseProvider)
        }
    }

    val requestQueue: RequestQueue
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(applicationContext)
            }
            return mRequestQueue as RequestQueue
        }

    fun <T> addToRequestQueue(req: Request<T>, tag: String?) {
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue.add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        Log.e(TAG, "AppController.addToRequestQueue() $req")
        requestQueue.add(req)
    }

    fun cancelPendingRequests(tag: Any?) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    companion object {
        val TAG = AppController::class.java.simpleName

        @get:Synchronized
        var instance: AppController? = null
            private set

        @get:Synchronized
        var sharedPreference: SharedPreference? = null


        var simpleCache: SimpleCache? = null
        var leastRecentlyUsedCacheEvictor: LeastRecentlyUsedCacheEvictor? = null
        var exoDatabaseProvider: ExoDatabaseProvider? = null
        var exoPlayerCacheSize: Long = 90 * 1024 * 1024
    }
}