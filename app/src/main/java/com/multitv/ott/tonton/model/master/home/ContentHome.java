package com.multitv.ott.tonton.model.master.home;


import java.io.Serializable;
import java.util.List;

/**
 * Created by root on 24/10/16.
 */
public class ContentHome implements Serializable {

    public List<String> category_ids = null;
    public String id;
    public String title;
    public String des;
    public String media_type;
    public String source;
    public String likes_count;
    public String rating;
    public String watch;
    public String favorite_count;
    public String cat_thumb;
    public String duration;
    public String mature_content;
    public String created;
    public String status;
    public String content_mode;
    public String genre;
    public List<Thumb> thumbs = null;
    public String access_type;
    public List<Price> price = null;
    public String season_id;
    public String url;
    public String current_date;
    public String publish_date;
    public Thumbnail thumbnail;
    public boolean isPlaybackAllowed() {
        return true;
    }

    public class Thumb implements Serializable {
        public String name;
        public Thumb_ thumb;

        public class Thumb_ implements Serializable {
            public String small;
            public String medium;
            // public String large;
        }
    }

    public class Thumbnail implements Serializable {
        public String small;
        public String medium;
        //public String large;
    }

   /* public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ContentHome contentHomeO1 = (ContentHome) o;
        String id1 = this.id;
        String id2 = contentHomeO1.id;

        if (id1.equals(id2)) {
            return true;
        } else return false;
    }

    @Override
    public int hashCode() {
        int result = index != null ? index.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (des != null ? des.hashCode() : 0);
        result = 31 * result + (language_id != null ? language_id.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (source != null ? source.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
       *//* result = 31 * result + (price_type != null ? price_type.hashCode() : 0);*//*
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (thumb_url != null ? thumb_url.hashCode() : 0);
       *//* result = 31 * result + (likes != null ? likes.hashCode() : 0);*//*
        result = 31 * result + (rating != null ? rating.hashCode() : 0);
        result = 31 * result + (watch != null ? watch.hashCode() : 0);
        result = 31 * result + (favorite != null ? favorite.hashCode() : 0);
        result = 31 * result + (meta != null ? meta.hashCode() : 0);
        result = 31 * result + (keywords != null ? keywords.hashCode() : 0);
        result = 31 * result + (category_ids != null ? category_ids.hashCode() : 0);

        return result;
    }*/
}
