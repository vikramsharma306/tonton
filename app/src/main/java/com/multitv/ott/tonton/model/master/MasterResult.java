package com.multitv.ott.tonton.model.master;

/**
 * Created by arungoyal on 17/11/17.
 */

public class MasterResult {
    private String udatedevice;
    private String deviceinfo;
    private String version;
    private String version_check;
    private String social;
    private String login;
    private String add;
    private String forgot;
    private String otp_generate;
    private String verify_otp;
    private String menu;
    private String catlist;
    private String home;
    private String list;
    private String detail;
    private String user_behavior;
    private String recomended;
    private String like;
    private String dislike;
    private String subscribe;
    private String unsubscribe;
    private String comment_list;
    private String comment_add;
    private String rating;
    private String playlist;
    private String analytics;
    private String autosuggest;
    private String watchduration;
    private String userrelated;
    private String edit;
    private String addetail;
    private String search;
    private String channel_list;
    private String live;
    private String ifallowed;
    private String isplaybackallowed;
    private String contact_us;
    private String channel_detail;
    private String push_content;
    private String privacy_policy;
    private String t_c;
    private String favorite;

    private String subs_complete_order_onetime;
    private String subs_create_order_onetime;
    private String subs_package_list;
    private String subs_paytm_checksum;
    private String subs_paytm_verifychecksum;
    private String subs_user_subscriptions;
    private String subs_redeem_coupon;

    public String getSubs_redeem_coupon() {
        return subs_redeem_coupon;
    }

    public void setSubs_redeem_coupon(String subs_redeem_coupon) {
        this.subs_redeem_coupon = subs_redeem_coupon;
    }

    public String getSubs_complete_order_onetime() {
        return subs_complete_order_onetime;
    }

    public void setSubs_complete_order_onetime(String subs_complete_order_onetime) {
        this.subs_complete_order_onetime = subs_complete_order_onetime;
    }

    public String getSubs_create_order_onetime() {
        return subs_create_order_onetime;
    }

    public void setSubs_create_order_onetime(String subs_create_order_onetime) {
        this.subs_create_order_onetime = subs_create_order_onetime;
    }

    public String getSubs_package_list() {
        return subs_package_list;
    }

    public void setSubs_package_list(String subs_package_list) {
        this.subs_package_list = subs_package_list;
    }

    public String getSubs_paytm_checksum() {
        return subs_paytm_checksum;
    }

    public void setSubs_paytm_checksum(String subs_paytm_checksum) {
        this.subs_paytm_checksum = subs_paytm_checksum;
    }

    public String getSubs_paytm_verifychecksum() {
        return subs_paytm_verifychecksum;
    }

    public void setSubs_paytm_verifychecksum(String subs_paytm_verifychecksum) {
        this.subs_paytm_verifychecksum = subs_paytm_verifychecksum;
    }

    public String getSubs_user_subscriptions() {
        return subs_user_subscriptions;
    }

    public void setSubs_user_subscriptions(String subs_user_subscriptions) {
        this.subs_user_subscriptions = subs_user_subscriptions;
    }

    public String getChannel_list() {
        return channel_list;
    }

    public void setChannel_list(String channel_list) {
        this.channel_list = channel_list;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getUdatedevice() {
        return udatedevice;
    }

    public void setUdatedevice(String udatedevice) {
        this.udatedevice = udatedevice;
    }

    public String getDeviceinfo() {
        return deviceinfo;
    }

    public void setDeviceinfo(String deviceinfo) {
        this.deviceinfo = deviceinfo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion_check() {
        return version_check;
    }

    public void setVersion_check(String version_check) {
        this.version_check = version_check;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getForgot() {
        return forgot;
    }

    public void setForgot(String forgot) {
        this.forgot = forgot;
    }

    public String getOtp_generate() {
        return otp_generate;
    }

    public void setOtp_generate(String otp_generate) {
        this.otp_generate = otp_generate;
    }

    public String getVerify_otp() {
        return verify_otp;
    }

    public void setVerify_otp(String verify_otp) {
        this.verify_otp = verify_otp;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public String getCatlist() {
        return catlist;
    }

    public void setCatlist(String catlist) {
        this.catlist = catlist;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getList() {
        return list;
    }

    public void setList(String list) {
        this.list = list;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getUser_behavior() {
        return user_behavior;
    }

    public void setUser_behavior(String user_behavior) {
        this.user_behavior = user_behavior;
    }

    public String getRecomended() {
        return recomended;
    }

    public void setRecomended(String recomended) {
        this.recomended = recomended;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getDislike() {
        return dislike;
    }

    public void setDislike(String dislike) {
        this.dislike = dislike;
    }

    public String getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(String subscribe) {
        this.subscribe = subscribe;
    }

    public String getUnsubscribe() {
        return unsubscribe;
    }

    public void setUnsubscribe(String unsubscribe) {
        this.unsubscribe = unsubscribe;
    }

    public String getComment_list() {
        return comment_list;
    }

    public void setComment_list(String comment_list) {
        this.comment_list = comment_list;
    }

    public String getComment_add() {
        return comment_add;
    }

    public void setComment_add(String comment_add) {
        this.comment_add = comment_add;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPlaylist() {
        return playlist;
    }

    public void setPlaylist(String playlist) {
        this.playlist = playlist;
    }

    public String getAnalytics() {
        return analytics;
    }

    public void setAnalytics(String analytics) {
        this.analytics = analytics;
    }

    public String getAutosuggest() {
        return autosuggest;
    }

    public void setAutosuggest(String autosuggest) {
        this.autosuggest = autosuggest;
    }

    public String getWatchduration() {
        return watchduration;
    }

    public void setWatchduration(String watchduration) {
        this.watchduration = watchduration;
    }

    public String getUserrelated() {
        return userrelated;
    }

    public void setUserrelated(String userrelated) {
        this.userrelated = userrelated;
    }

    public String getEdit() {
        return edit;
    }

    public void setEdit(String edit) {
        this.edit = edit;
    }

    public String getAddetail() {
        return addetail;
    }

    public void setAddetail(String addetail) {
        this.addetail = addetail;
    }


    public String getLive() {
        return live;
    }

    public void setLive(String live) {
        this.live = live;
    }

    public String getIfallowed() {
        return ifallowed;
    }

    public void setIfallowed(String ifallowed) {
        this.ifallowed = ifallowed;
    }

    public String getIsplaybackallowed() {
        return isplaybackallowed;
    }

    public void setIsplaybackallowed(String isplaybackallowed) {
        this.isplaybackallowed = isplaybackallowed;
    }


    public String getContactUs() {
        return contact_us;
    }

    public void setContactUs(String contactus) {
        this.contact_us = contactus;
    }

    public String getChannel_detail() {
        return channel_detail;
    }

    public void setChannel_detail(String channel_detail) {
        this.channel_detail = channel_detail;
    }

    public String getPush_content() {
        return push_content;
    }

    public void setPush_content(String push_content) {
        this.push_content = push_content;
    }

    public String getPrivacy_policy() {
        return privacy_policy;
    }

    public void setPrivacy_policy(String privacy_policy) {
        this.privacy_policy = privacy_policy;
    }

    public String getT_c() {
        return t_c;
    }

    public void setT_c(String t_c) {
        this.t_c = t_c;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }
}
