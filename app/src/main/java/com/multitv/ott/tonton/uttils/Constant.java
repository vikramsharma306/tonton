package com.multitv.ott.tonton.uttils;

import android.text.SpannableString;

public class Constant {

    public static final int CONTENT_TYPE_MULTITV_PLAYER_ACTIVITY = 0;
    public static final int CONTENT_TYPE_SONY_PLAYER_ACTIVITY = 1;
    public static final int CONTENT_TYPE_VIU_PLAYER_ACTIVITY = 2;


    public static final String CONTENT_ID_KEY = "content_id";
    public static final String IS_SPECIAL_CONTENT = "is_special";
    public static final String CONTENT_TYPE_MULTITV = "video_type";
    public static final String NOTIFICIATION_KEY = "notification_content";
    public static final String NOTIFICIATION_SERIOUS_KEY = "serious";
    public static final String NOTIFICIATION_NORMAL_KEY = "normal";
    public static final String NOTIFICIATION_CONDITION_KEY = "condition";
    public static final String PUSH_KEY_CRITIAL_CONDITION = "critical";
    public static final String KEY_PUSH_DISASTER_CONDITION = "disaster_key";
    public static final String KEY_USER_ALIVE_ON_PLAYER_SCREEN = "user_play_content";
    public static final String TIMESTAMP = "timestamp";

    public static final String EXTRA_KEY = "EXTRA_KEY";
    public static final String ONESIGNAL_KEY = "oneSignalId";
    public static final String ONESIGNAL_OLD_KEY = "oneSignalIdOld";

    public static final String EXTRA_REMINDER_DATA_KEY = "EXTRA_REMINDER_DATA_KEY";
    public static final String EXTRA_ICON_URL = "EXTRA_ICON_URL";
    public static final String EXTRA_NAME = "EXTRA_NAME";
    public static final String EXTRA_OPEN_HOME_SCREEN = "EXTRA_OPEN_HOME_SCREEN";
    public static SpannableString HTMLTEXT;
    public final static int TAB_SELECT_THARASH_HOLD = 8;
    public static int mTabSelectedCount = 0;

    public static final String EXTRA_CATEGORY_TYPE = "CATEGORY_TYPE";
    public static final String EXTRA_SEARCH_KEYWOARD = "SEARCH_KEYWORD";
    public static final String IS_FROM_SEARCH = "IS_FROM_SEARCH";

    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String EXTRA_SEARCH_TEXT = "SEARCH_TEXT";
    public static final int TYPE_UNIVERSAL_SEARCH = 1;
    public static final int TYPE_MOVIE = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_LIVE = 4;
    public static final int TYPE_TV_SHOWS = 5;
    public static final int TYPE_DEFAULT = 6;
    public static final int TYPE_SONY_LIV = 7;
    public static final int TYPE_VIMEO = 8;
    public static final int TYPE_VIU = 8;

    public static final String VIDEO_ID = "VIDEO_ID";

    public static final String EXTRA_DATE = "EXTRA_DATE";

    public static final String URI_KEY = "MKRKEY:";
    public static final String EXTRA_LIVE_TV_ID = "EXTRA_LIVE_TV_ID";
    public static final String EXTRA_IS_15_MIN_REMINDER_SET = "EXTRA_IS_15_MIN_REMINDER_SET";

    public static String EXTRA_CONTENT_INFO = "contentInfo";
    public static String EXTRA_CONTENT_OBJECT_STRING = "contentObject";
    public static String EXTRA_THUMBNAIL_PATH = "thumbnailPath";
    public static String EXTRA_NOTIFICATION_ID = "notificationId";

    public static final String EXTRA_SHOW_LIVE_TAB = "EXTRA_SHOW_LIVE_TAB";

    public static final int EXTRA_VALUE_WATCH = 0;
    public static final int EXTRA_VALUE_FAV = 1;
    public static final int EXTRA_VALUE_LIKE = 2;
    public static final int EXTRA_VALUE_RATE = 3;

    public static final String CONTENT_TYPE = "CONTENT_TYPE";
    public static final int TYPE_MOVIES = 1;

    public static String foldername = "veqtaprofile";

    public static String EXTRA_ACKNOWLEDGE_TYPE = "acknowledge_type";
    public static String EXTRA_UPDATE_UNREAD_COUNT = "update_unread_notification_count";

    public static final String PREFS_NAME = "Content_APP";
    public static final String FAVORITES = "Content_Favorite";
    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";
    // User name (make variable public to access from outside)
    public static final String KEY_NUMBER = "number";

    public static final String IS_TRIVIA_EXIST = "IsTriviaExist";
    String CACHE_TRIVIA_KEY = "Trivia";

    public static final String PLAYLIST_KEY = "playlist";
    public static final String FROM_WHERE_KEY = "from_where";
    public static final String OFFSET_KEY = "offset";
    public static final String TOTAL_COUNT_KEY = "total_count";
    public static final String INDEX_KEY = "index";
    public static final String PARENT_CATEGORY_ID = "parentId";
    public static final String KEY_FREE_DAY_COUNT = "free_days";

    public static final String DISCOUNT_COUPON_CODE = "discount_coupon_coded";
    public static final String DISCOUNT_COUPON_PRICE = "discount_coupon_price";

    public static final String BANNER_WIDTH = "w_640";
    public static final String BANNER_HEIGHT = "h_360";
    public static final String BANNER_QUALITY = "q_50";
    public static final String THUMBNAIL_WIDTH = "w_480";
    public static final String THUMBNAIL_HEIGHT = "h_270";
    public static final String THUMBNAIL_QUALITY = "q_40";

    public static final String LIVE_API_UPDATE_BROADCAST = "live_api_update_broadcast";

    public static final String SECRET_KEY = "mul123";
    public static final String SECURE_BASE_URL = "http://multitvcdn.vo.llnwd.net/secure/";

    public static final int totalXMinute = 0;

    //paytm
    public static final String PAYTM_MID = "DigItw81713600160654";
    public static final String PAYTM_WEBSITE = "DigItwWAP";
    public static final String PAYTM_CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";

    //Master
    public static final String UPDATE_DEVICE = "update_device";
    public static final String IS_UPDATE_DEVICE_ENC = "is_update_device_enc";

    public static final String DEVICE_INFO = "device_info";
    public static final String IS_DEVICE_INFO_ENC = "is_device_info_enc";

    public static final String CONTENT_VERSION = "content_version";
    public static final String IS_CONTENT_VERSION_ENC = "is_content_version_enc";

    public static final String APP_VERSION_CHECK = "app_version_check";
    public static final String IS_APP_VERSION_ENC = "is_app_version_check_enc";


    public static final String SOCIAL_LOGIN = "social_login";
    public static final String IS_SOCIAL_LOGIN_ENC = "is_social_login_enc";


    public static final String LOGIN = "login_api";
    public static final String IS_LOGIN_ENC = "is_login_api_enc";


    public static final String SIGNUP = "signup_api";
    public static final String IS_SIGNUP_ENC = "is_signup_api_enc";


    public static final String FORGOT_PASSWORD = "forgot_pass";
    public static final String IS_FORGOT_PASSWORD_ENC = "is_forgot_pass_enc";


    public static final String OTP_GENERATE = "otp_generate";
    public static final String IS_OTP_GENERATE_ENC = "is_otp_generate_enc";


    public static final String VERIFY_OTP = "verify_otp";
    public static final String IS_VERIFY_OTP_ENC = "is_verify_otp_enc";


    public static final String CMS_LINKS = "menu_api";
    public static final String IS_CMS_LINKS_ENC = "is_menu_api_enc";


    public static final String CAT_LIST = "catlist_api";
    public static final String IS_CAT_LIST_ENC = "is_catlist_api_enc";


    public static final String HOME_API = "home_api";
    public static final String IS_HOME_API_ENC = "is_home_api_enc";


    public static final String CONTENT_LIST = "content_list";
    public static final String IS_CONTENT_LIST_ENC = "is_content_list_enc";


    public static final String CONTENT_DETAIL = "content_detail";
    public static final String IS_CONTENT_DETAIL_ENC = "is_content_detail_enc";


    public static final String USER_BEHAVIOUR = "user_behavior";
    public static final String IS_USER_BEHAVIOUR_ENC = "is_user_behavior_enc";


    public static final String RECOMMENDED_API = "recomended_api";
    public static final String IS_RECOMMENDED_API_ENC = "is_recomended_api_enc";


    public static final String LIKE_API = "like_api";
    public static final String IS_LIKE_API_ENC = "is_like_api_enc";


    public static final String DISLIKE_API = "dislike_api";
    public static final String IS_DISLIKE_API_ENC = "is_dislike_api_enc";


    public static final String SUBSCRIBE_API = "subscribe_api";
    public static final String IS_SUBSCRIBE_API_ENC = "is_subscribe_api_enc";


    public static final String UNSUBSCRIBE_API = "unsubscribe_api";
    public static final String IS_UNSUBSCRIBE_API_ENC = "is_unsubscribe_api_enc";


    public static final String GET_COMMENT = "comment_list";
    public static final String IS_GET_COMMENT_ENC = "is_comment_list_enc";

    public static final String POST_COMMENT = "comment_add";
    public static final String IS_POST_COMMENT_ENC = "is_comment_add_enc";


    public static final String ADD_RATING = "rating_api";
    public static final String IS_ADD_RATING_ENC = "is_rating_api_enc";


    public static final String PLAYLIST_API = "playlist_api";

    public static final String IS_PLAYLIST_API_ENC = "is_playlist_api_enc";

    public static final String ANALYTICS_API = "analytics_api";
    public static final String IS_ANALYTICS_API_ENC = "is_analytics_api_enc";


    public static final String AUTO_SUGGEST = "autosuggest_api";
    public static final String IS_AUTO_SUGGEST_ENC = "is_autosuggest_api_enc";


    public static final String WATCH_DURATION = "watchduration_api";
    public static final String IS_WATCH_DURATION_ENC = "is_watchduration_api_enc";


    public static final String USER_RELATED = "userrelated_api";
    public static final String IS_USER_RELATED_ENC = "is_userrelated_api_enc";

    public static final String UPDATE_PROFILE = "update_profile";
    public static final String IS_UPDATE_PROFILE_enc = "is_update_profile_enc";

    public static final String AD_DETAILS = "addetail_api";
    public static final String IS_AD_DETAILS_ENC = "is_addetail_api_enc";

    public static final String SEARCH_API = "search_api";
    public static final String IS_SEARCH_API_ENC = "is_search_api_enc";

    public static final String CHANNEL_LIST = "channel_list";
    public static final String IS_CHANNEL_LIST_ENC = "is_channel_list_enc";

    public static final String LIVE_API = "live_api";
    public static final String IS_LIVE_API_ENC = "is_live_api_enc";

    public static final String IF_ALLOWED_API = "if_allowed_api";
    public static final String IF_ALLOWED_API_ENC = "if_allowed_api_enc";

    public static final String IS_PLAYBACK_ALLOWED_API = "is_play_allowed_api";
    public static final String IS_PLAYBACK_ALLOWED_API_ENC = "is_play_allowed_api_enc";

    public static final String CONTACT_US_API = "contact_us_api";
    public static final String IS_CONTACT_US_API_ENC = "is_contact_us_api_enc";

    public static final String CHANNEL_DETAIL_API = "channel_detail_api";
    public static final String IS_CHANNEL_DETAIL_API_ENC = "is_channel_detail_api_enc";

    public static final String PUSH_CONTENT_API = "push_content_api";
    public static final String IS_PUSH_CONTENT_API_ENC = "is_push_content_api_enc";

    public static final String PRIVACY_POLICY = "privacy_policy";
    public static final String IS_PRIVACY_POLICY_ENC = "is_push_content_api_enc";

    public static final String T_C = "t_c";
    public static final String IS_T_C_ENC = "is_push_content_api_enc";

    public static final String FAVORITE_API = "favorite_api";
    public static final String IS_FAVORITE_API_ENC = "is_favorite_api_enc";


    //################### SUBSCRIPTION API TAG ###########################

    public static final String SUBSCRIPTION_PACKAGE_LIST_API = "subs_package_list";
    public static final String IS_SUBSCRIPTION_PACKAGE_LIST_API_ENC = "is_subs_package_list_enc";

    public static final String USER_SUBS_PACKAGE_API = "subs_user_subscriptions";
    public static final String IS_USER_SUBS_PACKAGE_API_ENC = "is_subs_user_subscriptions_enc";

    public static final String SUBS_ONETIME_CREATE_ORDER_API = "subs_create_order_onetime";
    public static final String IS_SUBS_ONETIME_CREATE_ORDER_API_ENC = "is_subs_create_order_onetime_enc";

    public static final String SUBS_ONETIME_COMPLETE_ORDER_API = "subs_complete_order_onetime";
    public static final String IS_SUBS_ONETIME_COMPLETE_ORDER_API_ENC = "is_subs_complete_order_onetime_enc";

    public static final String SUBS_PAYTM_CHECKSUM_API = "subs_paytm_checksum";
    public static final String IS_SUBS_PAYTM_CHECKSUM_API_ENC = "is_subs_paytm_checksum_enc";

    public static final String SUBS_VERIFY_CHECKSUM_API = "subs_paytm_verifychecksum";
    public static final String IS_SUBS_VERIFY_CHECKSUM_API_ENC = "is_subs_paytm_verifychecksum_enc";

    public static final String SUBSCRIPTION_PACKAGE_END_DATE = "subs_finish_date";
    public static final String SUBSCRIPTION_PACKAGE_START_DATE = "subscription_package_start_date";
    public static final String SUBSCRIPTION_PACKAGE_ID = "subscription_package_id";
    public static final String SUBSCRIPTION_PACKAGE_PRICE = "subscription_package_price";
    public static final String SUBSCRIPTION_PACKAGE_NAME = "subscription_package_name";
    public static final String SUBSCRIPTION_VALIDATION_EXPRIY = "package_expriy-date";



    public static final String IS_SUBSCRIBED_USER = "IS_SUBSCRIBED_USER";



    public static final String SUBS_REDEEM_COUPON_API = "subs_redeem_coupon";
    public static final String IS_REDEEM_COUPON_API_API_ENC = "is_subs_redeem_coupon_enc";


    public static final String CONTENT_ID = "content_id";
}
