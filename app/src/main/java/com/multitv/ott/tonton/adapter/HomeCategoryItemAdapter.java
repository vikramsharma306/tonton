package com.multitv.ott.tonton.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.multitv.ott.tonton.R;
import com.multitv.ott.tonton.listeners.ContentDetailListener;
import com.multitv.ott.tonton.model.master.home.ContentHome;
import com.multitv.ott.tonton.uttils.ImageUtils;
import com.multitv.ott.tonton.uttils.ScreenUtils;
import com.multitv.ott.tonton.uttils.Tracer;

import java.util.ArrayList;


/**
 * Created by cyberlinks on 17/1/17.
 */

public class HomeCategoryItemAdapter extends RecyclerView.Adapter<HomeCategoryItemAdapter.ItemViewHolder> {
    private Activity mContext;
    private ArrayList<ContentHome> displayCategoryList;
    // private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private String totalCount, offset, cat_id;

    private ContentDetailListener contentDetailListener;

    public HomeCategoryItemAdapter(Activity context, ArrayList<ContentHome> disCategoryList, RecyclerView recyclerView, String offset, String totalCount, String cat_id, ContentDetailListener playLiveVideoListener) {
        mContext = context;
        this.offset = offset;
        this.totalCount = totalCount;
        this.cat_id = cat_id;
        this.displayCategoryList = disCategoryList;
        this.contentDetailListener = playLiveVideoListener;
/*
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();
                try {
                    lastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
//                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Tracer.error("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }

                    loading = true;
                }
            }
        });
*/
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.content_adapter_raw, parent, false));
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, mContext.getResources().getDisplayMetrics());

        holder.cardView.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) / 3 - widthAndHeightOfIcon, RecyclerView.LayoutParams.WRAP_CONTENT));

        int height = ScreenUtils.getScreenWidth(mContext) / 2 - widthAndHeightOfIcon;
        //int height = width * 9 / 16;
        holder.thumbnailImg.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height));


        ContentHome contentHome = displayCategoryList.get(position);


        String imageUrl = ImageUtils.getFinalImageUrlThumbnail(contentHome);
        if (!TextUtils.isEmpty(imageUrl)) {
            holder.thumbnailImg.setImageURI(imageUrl);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contentDetailListener.onContentClick(contentHome.id);
            }
        });

    }

    @Override
    public int getItemCount() {
        return displayCategoryList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        private SimpleDraweeView thumbnailImg;

        public ItemViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            thumbnailImg = view.findViewById(R.id.video_thumbnail_iv);
        }
    }

   /* public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/


}
