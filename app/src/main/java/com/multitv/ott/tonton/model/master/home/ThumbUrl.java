package com.multitv.ott.tonton.model.master.home;

import java.io.Serializable;

/**
 * Created by naseeb on 6/15/2017.
 */

public class ThumbUrl implements Serializable {
    public String base_path;
    public String thumb_path;
}
