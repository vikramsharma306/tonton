package com.multitv.ott.tonton.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class ThinTextView extends AppCompatTextView {

    public ThinTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ThinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ThinTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/thin.ttf");
        setTypeface(tf);
    }

}