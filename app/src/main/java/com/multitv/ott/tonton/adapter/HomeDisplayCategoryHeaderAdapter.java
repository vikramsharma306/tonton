package com.multitv.ott.tonton.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.multitv.ott.tonton.R;
import com.multitv.ott.tonton.listeners.ContentDetailListener;
import com.multitv.ott.tonton.listeners.HomeCategoryAddListener;
import com.multitv.ott.tonton.model.master.home.ContentHome;
import com.multitv.ott.tonton.model.master.home.Home_category;

import java.util.ArrayList;


/**
 * Created by cyberlinks on 18/1/17.
 */
public class HomeDisplayCategoryHeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private ArrayList<Home_category> displayList = new ArrayList<>();
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private HomeCategoryAddListener homeLoadDataInterface;

    public HomeDisplayCategoryHeaderAdapter(Context context, ArrayList<Home_category> displayList
            , RecyclerView recyclerView, HomeCategoryAddListener homeLoadDataInterface) {
        mContext = context;
        this.displayList = displayList;
        this.homeLoadDataInterface = homeLoadDataInterface;

/*
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();

                try {
                    // lastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Tracer.error("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!isLoading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                        Tracer.error("HomeAllListAdapter", "LoadMore Calling");
                    }
                    isLoading = true;
                }
            }
        });
*/

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.home_adapter_top_raw_layout, parent, false);
        return new ItemViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.textView.setText(displayList.get(position).cat_name);
        String cat_id = displayList.get(position).cat_id;
        ArrayList<ContentHome> contentHomes = displayList.get(position).cat_cntn;
        homeLoadDataInterface.setHomeCategoryDataOnLoadMoreCall(itemViewHolder.recyclerView, itemViewHolder.load_more_progress_bar, position, cat_id, contentHomes);

        itemViewHolder.moreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*    Intent intent = new Intent(mContext, MoreActivity.class);
                intent.putExtra("more_data_tag", 3);
                intent.putExtra("catName", displayList.get(itemPosition).cat_name);
                intent.putExtra("cat_id", displayList.get(itemPosition).cat_id);
                mContext.startActivity(intent);*/
            }
        });
    }


    @Override
    public int getItemCount() {
        return displayList.size(); //(1 is for vod sports);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        private RecyclerView recyclerView;
        private TextView textView;
        private TextView moreBtn;
        private ProgressBar load_more_progress_bar;


        public ItemViewHolder(View view) {
            super(view);
            recyclerView = view.findViewById(R.id.recycler_view_list);
            textView = view.findViewById(R.id.header_name);
            load_more_progress_bar = view.findViewById(R.id.load_more_progress_bar);
            moreBtn = view.findViewById(R.id.more_btn);
        }
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }


    public void setLoaded() {
        isLoading = false;
    }

   /* public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/

}