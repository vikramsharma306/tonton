package com.multitv.ott.tonton.customviews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class ItalicTextView extends AppCompatTextView {

    public ItalicTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ItalicTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ItalicTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/italic.ttf");
        setTypeface(tf);
    }

}