package com.multitv.ott.tonton.uttils;

import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class MultitvCipher {
    private String iv = "fedcba9876543210";
    private IvParameterSpec ivspec;
    private SecretKeySpec keyspec;
    private Cipher cipher;
    private String SecretKey = "0123456789abcdef";

    public MultitvCipher() {
        this.ivspec = new IvParameterSpec(this.iv.getBytes());
        this.keyspec = new SecretKeySpec(this.SecretKey.getBytes(), "AES");

        try {
            this.cipher = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException var2) {
            var2.printStackTrace();
        } catch (NoSuchPaddingException var3) {
            var3.printStackTrace();
        }

    }

    public byte[] encryptmyapi(String text) throws Exception {
        if (text != null && text.length() != 0) {
            Object var2 = null;

            try {
                this.cipher.init(1, this.keyspec, this.ivspec);
                byte[] encrypted = this.cipher.doFinal(padString(text).getBytes());
                return encrypted;
            } catch (Exception var4) {
                throw new Exception("[encrypt] " + var4.getMessage());
            }
        } else {
            throw new Exception("Empty string");
        }
    }

    public byte[] decryptmyapi(String code) throws Exception {
        if (code != null && code.length() != 0) {
            Object var2 = null;

            try {
                this.cipher.init(2, this.keyspec, this.ivspec);
                byte[] decrypted = this.cipher.doFinal(hexToBytes(code));
                return decrypted;
            } catch (Exception var4) {
                throw new Exception("[decrypt] " + var4.getMessage());
            }
        } else {
            throw new Exception("Empty string");
        }
    }

    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        } else {
            int len = data.length;
            String str = "";

            for(int i = 0; i < len; ++i) {
                if ((data[i] & 255) < 16) {
                    str = str + "0" + Integer.toHexString(data[i] & 255);
                } else {
                    str = str + Integer.toHexString(data[i] & 255);
                }
            }

            return str;
        }
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {
            int len = str.length() / 2;
            byte[] buffer = new byte[len];

            for(int i = 0; i < len; ++i) {
                buffer[i] = (byte)Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16);
            }

            return buffer;
        }
    }

    private static String padString(String source) {
        char paddingChar = ' ';
        int size = 16;
        int x = source.length() % size;
        int padLength = size - x;

        for(int i = 0; i < padLength; ++i) {
            source = source + paddingChar;
        }

        return source;
    }
}
