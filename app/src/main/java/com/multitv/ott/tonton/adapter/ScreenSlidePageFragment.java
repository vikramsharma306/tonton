package com.multitv.ott.tonton.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.facebook.drawee.view.SimpleDraweeView;
import com.multitv.ott.tonton.R;
import com.multitv.ott.tonton.model.master.home.ContentHome;

import java.util.ArrayList;
import java.util.List;

public class ScreenSlidePageFragment extends Fragment {

    SimpleDraweeView imageView;


    private ContentHome contentHome;
    private List<ContentHome> contentFeatureList;

    public static String EXTRA_FEATURED_CONTENT = "EXTRA_FEATURED_CONTENT";
    public static String EXTRA_FEATURED_CONTENT_LIST = "EXTRA_FEATURED_CONTENT_LIST";
    public static String EXTRA_FEATURED_POSITION = "EXTRA_FEATURED_CONTENT_LIST_POSITION";
    private int position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.slidingimages_layout, container, false);
        imageView = rootView.findViewById(R.id.sliding_image);

        if (getArguments() != null) {
            contentHome = (ContentHome) getArguments().getSerializable(EXTRA_FEATURED_CONTENT);
            contentFeatureList = (ArrayList<ContentHome>) getArguments().getSerializable(EXTRA_FEATURED_CONTENT_LIST);
            position = getArguments().getInt(EXTRA_FEATURED_POSITION, 0);
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String url = "";
        if (contentHome != null && contentHome.thumbs != null && !contentHome.thumbs.isEmpty()) {
            ContentHome.Thumb thumb = contentHome.thumbs.get(0);
            if (thumb != null && thumb.thumb != null && !TextUtils.isEmpty(thumb.thumb.medium))
                url = thumb.thumb.medium;
        }


        imageView.setImageURI(url);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


   /* @Override
    public void onSuccessRazorPayCreateOrder(List<String> list) {
        Log.e("PaymentSelection", ">>>>>>>ORDER_CREATED startPayment() calling");
        if (list != null && list.size() > 0) {
            String gateway_ref_id = list.get(0);
            String orderId = list.get(1);
            this.orderId = orderId;
            String amount = list.get(2);


            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            if (!TextUtils.isEmpty(amount) && !amount.equalsIgnoreCase("0")) {
                double amountTotal = Double.parseDouble(amount);
                new StartPaymentRaazorPayPresenter(getActivity()).startPayment(gateway_ref_id, "" + amountTotal, true);
            } else {
                new StartPaymentRaazorPayPresenter(getActivity()).startPayment(gateway_ref_id, "0", false);
            }
        }
    }

    @Override
    public void onFailRazorPayPaymentCreateOrder() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        Toast.makeText(AppController.getInstance(), "Payment Failed, please try again.!!", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        Log.e(this.getClass().getName(), "transaction data=====>>> " + paymentData.getSignature());
        Log.e(this.getClass().getName(), "transaction id success=====>>> " + paymentData.getPaymentId());
        // One time
        String razorPayOrderId = paymentData.getOrderId();
        String payment_id = paymentData.getPaymentId();
        String signature = paymentData.getSignature();
        paymentMethodPresenter.razorPayCompleteOntimeOrder(razorPayOrderId, payment_id, "1", signature, orderId, null, false);
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        Toast.makeText(AppController.getInstance(), "Payment Failed, please try again.", Toast.LENGTH_SHORT).show();
    }

    private ProgressDialog progressDialog;

    private void showProgressDailog(String message) {

        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(message); // Setting Message
        progressDialog.setTitle("Veqta Sports"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
    }*/

}
