package com.multitv.ott.tonton.uttils
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.view.LayoutInflater
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import com.multitv.ott.tonton.R
import com.multitv.ott.tonton.listeners.OnAlertDialogButtonClicked
import kotlinx.android.synthetic.main.custom_login_alert_dialog.view.*


object AppUttils {


    fun showLoginAlertDialog(context: Context, onAlertDialogButtonClicked: OnAlertDialogButtonClicked){
        var layoutInflater = LayoutInflater.from(context);
        var view = layoutInflater.inflate(R.layout.custom_login_alert_dialog, null)

        var builder = AlertDialog.Builder(context)
        var alertDialog = builder.create()
        alertDialog.setView(view)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()

        view.yesButton.setOnClickListener{
            onAlertDialogButtonClicked.onPositiveButtonClicked()
            alertDialog.dismiss()
        }

        view.skipButton.setOnClickListener{
            onAlertDialogButtonClicked.onNegativeButtonClicked()
            alertDialog.dismiss()
        }

    }

    fun askMultiplePermision(ctx: Context) {
        Dexter.withActivity(ctx as Activity)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ).withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {/* ... */

                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {/* ... */
                    }
                }).check()
    }

    fun shareIntent(body : String, context: Context){
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type="text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, body)
        context.startActivity(Intent.createChooser(shareIntent,context.getString(R.string.send_to)))
    }

    fun askPermission(ctx: Context, permission: String, permission1: String) {
        Dexter.withActivity(ctx as Activity)
                .withPermissions(permission, permission1)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.isAnyPermissionPermanentlyDenied) {
                            askPermission(ctx, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    fun checkPermission(ctx: Context, permission: String): Boolean {
        val res = ctx.checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    fun checkSmsPermission(ctx: Context, permission: String): Boolean {
        val res = ctx.checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }


    fun checkReadStoragePermission(ctx: Context, permission: String): Boolean {
        val res = ctx.checkCallingOrSelfPermission(permission)
        return res == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    fun isNetworkConnected(ctx: Context): Boolean {
        val cm = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        return cm.activeNetworkInfo != null
    }

}



