package com.multitv.ott.tonton.model.master.home;

import java.io.Serializable;

public class Price implements Serializable {
    public String amount;
    public String currency;
    public String discount_amt;
    public String duration;
    public String duration_type;
    public String type;
}
