package com.multitv.ott.tonton.activitys

import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager

import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.multitv.ott.tonton.R
import com.multitv.ott.tonton.presenter.MasterDataListener
import com.multitv.ott.tonton.presenter.MasterDataPresenterImpl
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity(), MediaPlayer.OnPreparedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.app_tool_bar_color)
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        getWindow().setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_splash)
        var uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.splash)
        videoView.setVideoURI(uri)
        videoView.start()
        videoView.setOnPreparedListener(this)

        getMasterData()
    }

    override fun onPrepared(mp: MediaPlayer?) {
        mp?.setLooping(true)
    }

    private fun getMasterData() {
        progressBar.visibility = View.VISIBLE
        MasterDataPresenterImpl(this, object : MasterDataListener {
            override fun onMasterDataSuccess(
                nType: String?,
                androidAppVersion: String?,
                timeStamp: Long
            ) {

                progressBar.visibility = View.GONE
                val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }

            override fun onMasterDataFail() {
                progressBar.visibility = View.GONE
                finish()
            }

        }).getMasterData()
    }
}