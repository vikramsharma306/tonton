package com.multitv.ott.tonton.adapter;

/**
 * Created by Created by Sunil on 09-08-2016.
 */

import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.multitv.ott.tonton.R;
import com.multitv.ott.tonton.listeners.ContentDetailListener;
import com.multitv.ott.tonton.model.master.home.ContentHome;
import com.multitv.ott.tonton.uttils.ImageUtils;
import com.multitv.ott.tonton.uttils.ScreenUtils;
import com.multitv.ott.tonton.uttils.Tracer;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class LiveHomeChannelAdapter extends RecyclerView.Adapter<LiveHomeChannelAdapter.SingleItemRowHolder> {

    private List<ContentHome> liveList;
    private Context mContext;
    private Date currentTime, date;
    // private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private Calendar calendar;
    private ContentDetailListener playLiveVideoListener;


    public LiveHomeChannelAdapter(Context context, List<ContentHome> liveList, RecyclerView recyclerView, ContentDetailListener playLiveVideoListener) {
        this.liveList = liveList;
        this.mContext = context;
        this.playLiveVideoListener = playLiveVideoListener;

//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                totalItemCount = recyclerView.getAdapter().getItemCount();
//                try {
//                    lastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
////                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
//                    // lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
//                } catch (Exception e) {
//                    Tracer.error("Error", "onScrolled: EXCEPTION " + e.getMessage());
//                    lastVisibleItem = 0;
//                }
//
//                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
//
//                    if (onLoadMoreListener != null) {
//                        loading = true;
//                        onLoadMoreListener.onLoadMore();
//                        Tracer.error("Live :", "LiveLoadMore:calling");
//                    }
//
//
//                }
//            }
//        });
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_live_content_adapter_row, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, final int position) {
        final ContentHome live = liveList.get(position);

        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, mContext.getResources().getDisplayMetrics());

        holder.cardView.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) / 2 - widthAndHeightOfIcon, RecyclerView.LayoutParams.WRAP_CONTENT));

        int width = ScreenUtils.getScreenWidth(mContext) / 2 - widthAndHeightOfIcon;
        int height = width * 9 / 16;
        holder.thumbnailImg.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, height));


        if (!TextUtils.isEmpty(ImageUtils.getFinalImageUrlThumbnailLive1(live)))
            holder.thumbnailImg.setImageURI(ImageUtils.getFinalImageUrlThumbnailLive1(live));


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playLiveVideoListener.onContentClick(live.id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return liveList.size();
    }

    static class SingleItemRowHolder extends RecyclerView.ViewHolder {
        protected CardView cardView;
        protected SimpleDraweeView thumbnailImg;


        public SingleItemRowHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            thumbnailImg = view.findViewById(R.id.content_icon);
        }
    }

    /*public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/


}