package com.multitv.ott.tonton.presenter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.text.TextUtils;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.multitv.ott.tonton.R;
import com.multitv.ott.tonton.apirequest.ApiRequestHelper;
import com.multitv.ott.tonton.appcontroller.AppController;
import com.multitv.ott.tonton.model.master.MasterResult;
import com.multitv.ott.tonton.uttils.Constant;
import com.multitv.ott.tonton.uttils.Json;
import com.multitv.ott.tonton.uttils.Tracer;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by naseeb on 11/8/2017.
 */

public class MasterDataPresenterImpl implements MasterDataPresenter {

    private Activity activity;
    private MasterDataListener masterDataListener;

    public MasterDataPresenterImpl(Activity activity,
                                   MasterDataListener masterDataListener) {
        this.activity = activity;
        this.masterDataListener = masterDataListener;
    }

    @Override
    public void getMasterData() {
        try {
            PackageInfo packageInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            final String appVersion = packageInfo.versionName;
            if (activity == null || TextUtils.isEmpty(appVersion))
                return;

            /*if (PreferenceData.getVersionData(activity, appVersion).isEmpty()) {*/
            String masterUrl = ApiRequestHelper.MASTER_URL + "/device/android"; //+ "/app_version/" + appVersion
            Tracer.error(this.getClass().getName(), "Master URL is " + masterUrl);

            StringRequest jsonObjReq = new StringRequest(Request.Method.GET,
                    masterUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    try {
                        JSONObject mObj = new JSONObject(response);
                        String nType = mObj.optString("n_type");
                        String androidAppVersion = mObj.optString("android_v");
                        long timeStmap = mObj.optLong("timestamp");
                        if (mObj.optInt("code") == 1) {
                            String str = mObj.optString("result");
                            Tracer.error(this.getClass().getName(), "Master_api_response" + "code 1");
                            if (!TextUtils.isEmpty(str)) {
                                MasterResult master = Json.parse(str.trim(), MasterResult.class);
                                if (master != null) {
                                    //PreferenceData.setVersionData(activity, str, appVersion);

                                    SharedPreferences sharedPreferences = activity.getSharedPreferences(Constant.PREFS_NAME, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    if (master.getUdatedevice() != null) {
                                        String updateDevice[] = master.getUdatedevice().split("\\|,");
                                        if (updateDevice.length >= 2) {
                                            editor.putString(Constant.IS_UPDATE_DEVICE_ENC, updateDevice[0]);
                                            editor.putString(Constant.UPDATE_DEVICE, updateDevice[1]);
                                        } else {
                                            editor.putString(Constant.IS_UPDATE_DEVICE_ENC, updateDevice[0]);
                                        }
                                    }

                                    if (master.getDeviceinfo() != null) {
                                        String deviceInfo[] = master.getDeviceinfo().split("\\|,");
                                        if (deviceInfo.length >= 2) {
                                            editor.putString(Constant.IS_DEVICE_INFO_ENC, deviceInfo[0]);
                                            editor.putString(Constant.DEVICE_INFO, deviceInfo[1]);
                                        } else {
                                            editor.putString(Constant.IS_DEVICE_INFO_ENC, deviceInfo[0]);
                                        }
                                    }


                                    if (master.getVersion() != null) {
                                        String contentVersion[] = master.getVersion().split("\\|,");
                                        if (contentVersion.length >= 2) {
                                            editor.putString(Constant.IS_CONTENT_VERSION_ENC, contentVersion[0]);
                                            editor.putString(Constant.CONTENT_VERSION, contentVersion[1]);
                                        } else {
                                            editor.putString(Constant.IS_CONTENT_VERSION_ENC, contentVersion[0]);
                                        }
                                    }


                                    if (master.getVersion_check() != null) {
                                        String versionCheck[] = master.getVersion_check().split("\\|,");
                                        if (versionCheck.length >= 2) {
                                            editor.putString(Constant.IS_APP_VERSION_ENC, versionCheck[0]);
                                            editor.putString(Constant.APP_VERSION_CHECK, versionCheck[1]);
                                        } else {
                                            editor.putString(Constant.IS_APP_VERSION_ENC, versionCheck[0]);
                                        }
                                    }


                                    if (master.getSocial() != null && master.getSocial().length() > 0) {
                                        String social[] = master.getSocial().split("\\|,");
                                        if (social.length >= 2) {
                                            editor.putString(Constant.IS_SOCIAL_LOGIN_ENC, social[0]);
                                            editor.putString(Constant.SOCIAL_LOGIN, social[1]);
                                        } else {
                                            editor.putString(Constant.IS_SOCIAL_LOGIN_ENC, social[0]);
                                        }
                                    }


                                    if (master.getLogin() != null && master.getLogin().length() > 0) {
                                        String login[] = master.getLogin().split("\\|,");
                                        if (login.length >= 2) {
                                            editor.putString(Constant.IS_LOGIN_ENC, login[0]);
                                            editor.putString(Constant.LOGIN, login[1]);
                                        } else {
                                            editor.putString(Constant.IS_LOGIN_ENC, login[0]);
                                        }
                                    }


                                    if (master.getAdd() != null && master.getAdd().length() > 0) {
                                        String signup[] = master.getAdd().split("\\|,");
                                        if (signup.length >= 2) {
                                            editor.putString(Constant.IS_SIGNUP_ENC, signup[0]);
                                            editor.putString(Constant.SIGNUP, signup[1]);
                                        } else {
                                            editor.putString(Constant.IS_SIGNUP_ENC, signup[0]);
                                        }
                                    }


                                    if (master.getForgot() != null && master.getForgot().length() > 0) {
                                        String forgot[] = master.getForgot().split("\\|,");
                                        if (forgot.length >= 2) {
                                            editor.putString(Constant.IS_FORGOT_PASSWORD_ENC, forgot[0]);
                                            editor.putString(Constant.FORGOT_PASSWORD, forgot[1]);
                                        } else {
                                            editor.putString(Constant.IS_FORGOT_PASSWORD_ENC, forgot[0]);
                                        }

                                    }


                                    if (master.getOtp_generate() != null && master.getOtp_generate().length() > 0) {
                                        String otpGenerate[] = master.getOtp_generate().split("\\|,");
                                        if (otpGenerate.length >= 2) {
                                            editor.putString(Constant.IS_OTP_GENERATE_ENC, otpGenerate[0]);
                                            editor.putString(Constant.OTP_GENERATE, otpGenerate[1]);
                                        } else {
                                            editor.putString(Constant.IS_OTP_GENERATE_ENC, otpGenerate[0]);
                                        }
                                    }


                                    if (master.getVerify_otp() != null && master.getVerify_otp().length() > 0) {
                                        String verify[] = master.getVerify_otp().split("\\|,");
                                        if (verify.length >= 2) {
                                            editor.putString(Constant.IS_VERIFY_OTP_ENC, verify[0]);
                                            editor.putString(Constant.VERIFY_OTP, verify[1]);

                                        } else {
                                            editor.putString(Constant.VERIFY_OTP, verify[0]);
                                        }
                                    }

                                    if (master.getMenu() != null && master.getMenu().length() > 0) {
                                        String menu[] = master.getMenu().split("\\|,");
                                        if (menu.length >= 2) {
                                            editor.putString(Constant.IS_CMS_LINKS_ENC, menu[0]);
                                            editor.putString(Constant.CMS_LINKS, menu[1]);
                                        } else {
                                            editor.putString(Constant.IS_CMS_LINKS_ENC, menu[0]);
                                        }
                                    }


                                    if (master.getCatlist() != null && master.getCatlist().length() > 0) {
                                        String catList[] = master.getCatlist().split("\\|,");
                                        if (catList.length >= 2) {
                                            editor.putString(Constant.IS_CAT_LIST_ENC, catList[0]);
                                            editor.putString(Constant.CAT_LIST, catList[1]);
                                        } else {
                                            editor.putString(Constant.IS_CAT_LIST_ENC, catList[0]);
                                        }
                                    }


                                    if (master.getHome() != null && master.getHome().length() > 0) {
                                        String home[] = master.getHome().split("\\|,");
                                        if (home.length >= 2) {
                                            editor.putString(Constant.IS_HOME_API_ENC, home[0]);
                                            editor.putString(Constant.HOME_API, home[1]);
                                        } else {
                                            editor.putString(Constant.IS_HOME_API_ENC, home[0]);
                                        }
                                    }


                                    if (master.getList() != null && master.getList().length() > 0) {
                                        String contentList[] = master.getList().split("\\|,");
                                        if (contentList.length >= 2) {
                                            editor.putString(Constant.IS_CONTENT_LIST_ENC, contentList[0]);
                                            editor.putString(Constant.CONTENT_LIST, contentList[1]);
                                        } else {
                                            editor.putString(Constant.IS_CONTENT_LIST_ENC, contentList[0]);
                                        }
                                    }


                                    if (master.getDetail() != null && master.getDetail().length() > 0) {
                                        String details[] = master.getDetail().split("\\|,");
                                        if (details.length >= 2) {
                                            editor.putString(Constant.IS_CONTENT_DETAIL_ENC, details[0]);
                                            editor.putString(Constant.CONTENT_DETAIL, details[1]);
                                        } else {
                                            editor.putString(Constant.IS_CONTENT_DETAIL_ENC, details[0]);
                                        }
                                    }


                                    if (master.getUser_behavior() != null && master.getUser_behavior().length() > 0) {
                                        String userBehaviuor[] = master.getUser_behavior().split("\\|,");
                                        if (userBehaviuor.length >= 2) {
                                            editor.putString(Constant.IS_USER_BEHAVIOUR_ENC, userBehaviuor[0]);
                                            editor.putString(Constant.USER_BEHAVIOUR, userBehaviuor[1]);
                                        } else {
                                            editor.putString(Constant.IS_USER_BEHAVIOUR_ENC, userBehaviuor[0]);
                                        }
                                    }


                                    if (master.getRecomended() != null && master.getRecomended().length() > 0) {
                                        String[] recommended = master.getRecomended().split("\\|,");
                                        if (recommended.length >= 2) {
                                            editor.putString(Constant.IS_RECOMMENDED_API_ENC, recommended[0]);
                                            editor.putString(Constant.RECOMMENDED_API, recommended[1]);
                                        } else {
                                            editor.putString(Constant.IS_RECOMMENDED_API_ENC, recommended[0]);
                                        }
                                    }


                                    if (master.getLike() != null && master.getLike().length() > 0) {
                                        String[] like = master.getLike().split("\\|,");
                                        if (like.length >= 2) {
                                            editor.putString(Constant.IS_LIKE_API_ENC, like[0]);
                                            editor.putString(Constant.LIKE_API, like[1]);
                                        } else {
                                            editor.putString(Constant.IS_LIKE_API_ENC, like[0]);
                                        }
                                    }


                                    if (master.getDislike() != null && master.getDislike().length() > 0) {
                                        String dislike[] = master.getDislike().split("\\|,");
                                        if (dislike.length >= 2) {
                                            editor.putString(Constant.IS_DISLIKE_API_ENC, dislike[0]);
                                            editor.putString(Constant.DISLIKE_API, dislike[1]);
                                        } else {
                                            editor.putString(Constant.IS_DISLIKE_API_ENC, dislike[0]);
                                        }
                                    }


                                    if (master.getSubscribe() != null && master.getSubscribe().length() > 0) {
                                        String subscribe[] = master.getSubscribe().split("\\|,");
                                        if (subscribe.length >= 2) {
                                            editor.putString(Constant.IS_SUBSCRIBE_API_ENC, subscribe[0]);
                                            editor.putString(Constant.SUBSCRIBE_API, subscribe[1]);
                                        } else {
                                            editor.putString(Constant.IS_SUBSCRIBE_API_ENC, subscribe[0]);
                                        }
                                    }


                                    if (master.getUnsubscribe() != null && master.getUnsubscribe().length() > 0) {
                                        String[] unsubscribe = master.getUnsubscribe().split("\\|,");
                                        if (unsubscribe.length >= 2) {
                                            editor.putString(Constant.IS_UNSUBSCRIBE_API_ENC, unsubscribe[0]);
                                            editor.putString(Constant.UNSUBSCRIBE_API, unsubscribe[1]);
                                        } else {
                                            editor.putString(Constant.IS_UNSUBSCRIBE_API_ENC, unsubscribe[0]);
                                        }
                                    }


                                    if (master.getComment_list() != null && master.getComment_list().length() > 0) {
                                        String comment[] = master.getComment_list().split("\\|,");
                                        if (comment.length >= 2) {
                                            editor.putString(Constant.IS_GET_COMMENT_ENC, comment[0]);
                                            editor.putString(Constant.GET_COMMENT, comment[1]);
                                        } else {
                                            editor.putString(Constant.IS_GET_COMMENT_ENC, comment[0]);
                                        }
                                    }


                                    if (master.getComment_add() != null && master.getComment_add().length() > 0) {
                                        String commentAdd[] = master.getComment_add().split("\\|,");
                                        if (commentAdd.length >= 2) {
                                            editor.putString(Constant.IS_POST_COMMENT_ENC, commentAdd[0]);
                                            editor.putString(Constant.POST_COMMENT, commentAdd[1]);
                                        } else {
                                            editor.putString(Constant.IS_POST_COMMENT_ENC, commentAdd[0]);
                                        }
                                    }


                                    if (master.getRating() != null && master.getRating().length() > 0) {
                                        String[] rating = master.getRating().split("\\|,");
                                        if (rating.length >= 2) {
                                            editor.putString(Constant.IS_ADD_RATING_ENC, rating[0]);
                                            editor.putString(Constant.ADD_RATING, rating[1]);
                                        } else {
                                            editor.putString(Constant.IS_ADD_RATING_ENC, rating[0]);
                                        }
                                    }


                                    if (master.getPlaylist() != null && master.getPlaylist().length() > 0) {
                                        String playList[] = master.getPlaylist().split("\\|,");
                                        if (playList.length >= 2) {
                                            editor.putString(Constant.IS_PLAYLIST_API_ENC, playList[0]);
                                            editor.putString(Constant.PLAYLIST_API, playList[1]);
                                        } else {
                                            editor.putString(Constant.IS_PLAYLIST_API_ENC, playList[0]);
                                        }
                                    }

                                    if (master.getAnalytics() != null && master.getAnalytics().length() > 0) {

                                        String[] analytics = master.getAnalytics().split("\\|,");
                                        if (analytics.length >= 2) {
                                            editor.putString(Constant.IS_ANALYTICS_API_ENC, analytics[0]);
                                            editor.putString(Constant.ANALYTICS_API, analytics[1]);
                                        } else {
                                            editor.putString(Constant.IS_ANALYTICS_API_ENC, analytics[0]);
                                        }
                                    }

                                    if (master.getAutosuggest() != null && master.getAutosuggest().length() > 0) {
                                        String[] autoSuggest = master.getAutosuggest().split("\\|,");
                                        if (autoSuggest.length >= 2) {
                                            editor.putString(Constant.IS_AUTO_SUGGEST_ENC, autoSuggest[0]);
                                            editor.putString(Constant.AUTO_SUGGEST, autoSuggest[1]);
                                        } else {
                                            editor.putString(Constant.IS_AUTO_SUGGEST_ENC, autoSuggest[0]);
                                        }
                                    }


                                    if (master.getWatchduration() != null && master.getWatchduration().length() > 0) {
                                        String watchDuration[] = master.getWatchduration().split("\\|,");
                                        if (watchDuration.length >= 2) {
                                            editor.putString(Constant.IS_WATCH_DURATION_ENC, watchDuration[0]);
                                            editor.putString(Constant.WATCH_DURATION, watchDuration[1]);
                                        } else {
                                            editor.putString(Constant.IS_WATCH_DURATION_ENC, watchDuration[0]);
                                        }
                                    }


                                    if (master.getUserrelated() != null && master.getUserrelated().length() > 0) {
                                        String userRaleted[] = master.getUserrelated().split("\\|,");
                                        if (userRaleted.length >= 2) {
                                            editor.putString(Constant.IS_USER_RELATED_ENC, userRaleted[0]);
                                            editor.putString(Constant.USER_RELATED, userRaleted[1]);
                                        } else {
                                            editor.putString(Constant.IS_USER_RELATED_ENC, userRaleted[0]);
                                        }
                                    }


                                    if (master.getEdit() != null && master.getEdit().length() > 0) {
                                        String edit[] = master.getEdit().split("\\|,");
                                        if (edit.length >= 2) {
                                            editor.putString(Constant.IS_UPDATE_PROFILE_enc, edit[0]);
                                            editor.putString(Constant.UPDATE_PROFILE, edit[1]);
                                        } else {
                                            editor.putString(Constant.IS_UPDATE_PROFILE_enc, edit[0]);
                                        }
                                    }


                                    if (master.getAddetail() != null && master.getAddetail().length() > 0) {
                                        String adDetails[] = master.getAddetail().split("\\|,");
                                        if (adDetails.length >= 2) {
                                            editor.putString(Constant.IS_AD_DETAILS_ENC, adDetails[0]);
                                            editor.putString(Constant.AD_DETAILS, adDetails[1]);
                                        } else {
                                            editor.putString(Constant.IS_AD_DETAILS_ENC, adDetails[0]);
                                        }
                                    }


                                    if (master.getSearch() != null && master.getSearch().length() > 0) {
                                        String search[] = master.getSearch().split("\\|,");
                                        if (search.length >= 2) {
                                            editor.putString(Constant.IS_SEARCH_API_ENC, search[0]);
                                            editor.putString(Constant.SEARCH_API, search[1]);
                                        } else {
                                            editor.putString(Constant.IS_SEARCH_API_ENC, search[0]);
                                        }
                                    }


                                    if (master.getChannel_list() != null && master.getChannel_list().length() > 0) {
                                        String channelList[] = master.getChannel_list().split("\\|,");
                                        if (channelList.length >= 2) {
                                            editor.putString(Constant.IS_CHANNEL_LIST_ENC, channelList[0]);
                                            editor.putString(Constant.CHANNEL_LIST, channelList[1]);
                                        } else {
                                            editor.putString(Constant.IS_CHANNEL_LIST_ENC, channelList[0]);
                                        }
                                    }


                                    if (master.getLive() != null && master.getLive().length() > 0) {
                                        String liveList[] = master.getLive().split("\\|,");
                                        if (liveList.length >= 2) {
                                            editor.putString(Constant.IS_LIVE_API_ENC, liveList[0]);
                                            editor.putString(Constant.LIVE_API, liveList[1]);
                                        } else {
                                            editor.putString(Constant.IS_LIVE_API_ENC, liveList[0]);
                                        }
                                    }


                                    if (master.getIfallowed() != null && master.getIfallowed().length() > 0) {
                                        String liveList[] = master.getIfallowed().split("\\|,");
                                        if (liveList.length >= 2) {
                                            editor.putString(Constant.IF_ALLOWED_API_ENC, liveList[0]);
                                            editor.putString(Constant.IF_ALLOWED_API, liveList[1]);
                                        } else {
                                            editor.putString(Constant.IF_ALLOWED_API_ENC, liveList[0]);
                                        }
                                    }

                                    if (master.getIsplaybackallowed() != null && master.getIsplaybackallowed().length() > 0) {
                                        String liveList[] = master.getIsplaybackallowed().split("\\|,");
                                        if (liveList.length >= 2) {
                                            editor.putString(Constant.IS_PLAYBACK_ALLOWED_API_ENC, liveList[0]);
                                            editor.putString(Constant.IS_PLAYBACK_ALLOWED_API, liveList[1]);
                                        } else {
                                            editor.putString(Constant.IS_PLAYBACK_ALLOWED_API_ENC, liveList[0]);
                                        }
                                    }

                                    if (master.getContactUs() != null && master.getContactUs().length() > 0) {
                                        String contactList[] = master.getContactUs().split("\\|,");
                                        if (contactList.length >= 2) {
                                            editor.putString(Constant.IS_CONTACT_US_API_ENC, contactList[0]);
                                            editor.putString(Constant.CONTACT_US_API, contactList[1]);
                                        } else {
                                            editor.putString(Constant.IS_CONTACT_US_API_ENC, contactList[0]);
                                        }
                                    }

                                    if (master.getChannel_detail() != null && master.getChannel_detail().length() > 0) {
                                        String channelDetailList[] = master.getChannel_detail().split("\\|,");
                                        if (channelDetailList.length >= 2) {
                                            editor.putString(Constant.IS_CHANNEL_DETAIL_API_ENC, channelDetailList[0]);
                                            editor.putString(Constant.CHANNEL_DETAIL_API, channelDetailList[1]);
                                        } else {
                                            editor.putString(Constant.IS_CHANNEL_DETAIL_API_ENC, channelDetailList[0]);
                                        }
                                    }

                                    if (master.getPush_content() != null && master.getPush_content().length() > 0) {
                                        String pushContentList[] = master.getPush_content().split("\\|,");
                                        if (pushContentList.length >= 2) {
                                            editor.putString(Constant.IS_PUSH_CONTENT_API_ENC, pushContentList[0]);
                                            editor.putString(Constant.PUSH_CONTENT_API, pushContentList[1]);
                                        } else {
                                            editor.putString(Constant.IS_PUSH_CONTENT_API_ENC, pushContentList[0]);
                                        }
                                    }

                                    if (master.getPrivacy_policy() != null && master.getPrivacy_policy().length() > 0) {
                                        String privacyPolicyList[] = master.getPrivacy_policy().split("\\|,");
                                        if (privacyPolicyList.length >= 2) {
                                            editor.putString(Constant.IS_PRIVACY_POLICY_ENC, privacyPolicyList[0]);
                                            editor.putString(Constant.PRIVACY_POLICY, privacyPolicyList[1]);
                                        } else {
                                            editor.putString(Constant.IS_PRIVACY_POLICY_ENC, privacyPolicyList[0]);
                                        }
                                    }

                                    if (master.getT_c() != null && master.getT_c().length() > 0) {
                                        String tcList[] = master.getT_c().split("\\|,");
                                        if (tcList.length >= 2) {
                                            editor.putString(Constant.IS_T_C_ENC, tcList[0]);
                                            editor.putString(Constant.T_C, tcList[1]);
                                        } else {
                                            editor.putString(Constant.IS_T_C_ENC, tcList[0]);
                                        }
                                    }

                                    if (master.getFavorite() != null && master.getFavorite().length() > 0) {
                                        String favoriteList[] = master.getFavorite().split("\\|,");
                                        if (favoriteList.length >= 2) {
                                            editor.putString(Constant.IS_FAVORITE_API_ENC, favoriteList[0]);
                                            editor.putString(Constant.FAVORITE_API, favoriteList[1]);
                                        } else {
                                            editor.putString(Constant.IS_FAVORITE_API_ENC, favoriteList[0]);
                                        }
                                    }

                                    //############## SUBSCRIPTION API ####################

                                    if (master.getSubs_package_list() != null && master.getSubs_package_list().length() > 0) {
                                        String subscriptionPackageList[] = master.getSubs_package_list().split("\\|,");
                                        if (subscriptionPackageList.length >= 2) {
                                            editor.putString(Constant.IS_SUBSCRIPTION_PACKAGE_LIST_API_ENC, subscriptionPackageList[0]);
                                            editor.putString(Constant.SUBSCRIPTION_PACKAGE_LIST_API, subscriptionPackageList[1]);
                                        } else {
                                            editor.putString(Constant.IS_SUBSCRIPTION_PACKAGE_LIST_API_ENC, subscriptionPackageList[0]);
                                        }
                                    }

                                    if (master.getSubs_user_subscriptions() != null && master.getSubs_user_subscriptions().length() > 0) {
                                        String subscriptionUserPackage[] = master.getSubs_user_subscriptions().split("\\|,");
                                        if (subscriptionUserPackage.length >= 2) {
                                            editor.putString(Constant.IS_USER_SUBS_PACKAGE_API_ENC, subscriptionUserPackage[0]);
                                            editor.putString(Constant.USER_SUBS_PACKAGE_API, subscriptionUserPackage[1]);
                                        } else {
                                            editor.putString(Constant.IS_USER_SUBS_PACKAGE_API_ENC, subscriptionUserPackage[0]);
                                        }
                                    }


                                    if (master.getSubs_create_order_onetime() != null && master.getSubs_create_order_onetime().length() > 0) {
                                        String subscriptionOnetimeCreateOrder[] = master.getSubs_create_order_onetime().split("\\|,");
                                        if (subscriptionOnetimeCreateOrder.length >= 2) {
                                            editor.putString(Constant.IS_SUBS_ONETIME_CREATE_ORDER_API_ENC, subscriptionOnetimeCreateOrder[0]);
                                            editor.putString(Constant.SUBS_ONETIME_CREATE_ORDER_API, subscriptionOnetimeCreateOrder[1]);
                                        } else {
                                            editor.putString(Constant.IS_SUBS_ONETIME_CREATE_ORDER_API_ENC, subscriptionOnetimeCreateOrder[0]);
                                        }
                                    }


                                    if (master.getSubs_complete_order_onetime() != null && master.getSubs_complete_order_onetime().length() > 0) {
                                        String subscriptionOnetimeCompleteOrder[] = master.getSubs_complete_order_onetime().split("\\|,");
                                        if (subscriptionOnetimeCompleteOrder.length >= 2) {
                                            editor.putString(Constant.IS_SUBS_ONETIME_COMPLETE_ORDER_API_ENC, subscriptionOnetimeCompleteOrder[0]);
                                            editor.putString(Constant.SUBS_ONETIME_COMPLETE_ORDER_API, subscriptionOnetimeCompleteOrder[1]);
                                        } else {
                                            editor.putString(Constant.IS_SUBS_ONETIME_COMPLETE_ORDER_API_ENC, subscriptionOnetimeCompleteOrder[0]);
                                        }
                                    }


                                    if (master.getSubs_paytm_checksum() != null && master.getSubs_paytm_checksum().length() > 0) {
                                        String subscriptionPaytmChecksum[] = master.getSubs_paytm_checksum().split("\\|,");
                                        if (subscriptionPaytmChecksum.length >= 2) {
                                            editor.putString(Constant.IS_SUBS_PAYTM_CHECKSUM_API_ENC, subscriptionPaytmChecksum[0]);
                                            editor.putString(Constant.SUBS_PAYTM_CHECKSUM_API, subscriptionPaytmChecksum[1]);
                                        } else {
                                            editor.putString(Constant.IS_SUBS_PAYTM_CHECKSUM_API_ENC, subscriptionPaytmChecksum[0]);
                                        }
                                    }


                                    if (master.getSubs_paytm_verifychecksum() != null && master.getSubs_paytm_verifychecksum().length() > 0) {
                                        String subscriptionPaytmVerifyChecksum[] = master.getSubs_paytm_verifychecksum().split("\\|,");
                                        if (subscriptionPaytmVerifyChecksum.length >= 2) {
                                            editor.putString(Constant.IS_SUBS_VERIFY_CHECKSUM_API_ENC, subscriptionPaytmVerifyChecksum[0]);
                                            editor.putString(Constant.SUBS_VERIFY_CHECKSUM_API, subscriptionPaytmVerifyChecksum[1]);
                                        } else {
                                            editor.putString(Constant.IS_SUBS_VERIFY_CHECKSUM_API_ENC, subscriptionPaytmVerifyChecksum[0]);
                                        }
                                    }


                                    if (master.getSubs_redeem_coupon() != null && master.getSubs_redeem_coupon().length() > 0) {
                                        String redeemCoupon[] = master.getSubs_redeem_coupon().split("\\|,");
                                        if (redeemCoupon.length >= 2) {
                                            editor.putString(Constant.IS_REDEEM_COUPON_API_API_ENC, redeemCoupon[0]);
                                            editor.putString(Constant.SUBS_REDEEM_COUPON_API, redeemCoupon[1]);
                                        } else {
                                            editor.putString(Constant.IS_REDEEM_COUPON_API_API_ENC, redeemCoupon[0]);
                                        }
                                    }


                                    editor.commit();

                                    if (masterDataListener != null)
                                        masterDataListener.onMasterDataSuccess(nType, androidAppVersion, timeStmap);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        if (activity == null)
                            return;
                        Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_SHORT).show();

                        if (masterDataListener != null)
                            masterDataListener.onMasterDataFail();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Tracer.error(this.getClass().getName(), "Error: " + error.getMessage());

                    if (activity == null)
                        return;
                    Toast.makeText(activity, activity.getString(R.string.network_error), Toast.LENGTH_SHORT).show();

                    if (masterDataListener != null)
                        masterDataListener.onMasterDataFail();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    return params;
                }
            };
            AppController.Companion.getInstance().addToRequestQueue(jsonObjReq);
            /*} else if (masterDataListener != null)
                masterDataListener.onMasterDataSuccess();*/
        } catch (Exception e) {
            Tracer.error(this.getClass().getName(), "Error: " + e.getMessage());
        }
    }
}
